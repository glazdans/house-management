# House management 
Allows to define apartments, bill tarrifs and monthly water usage to generate utility bills for tenants. 
#### Description

#### How to build
###### Unix
```bash
./gradlew bootRepackage
```
###### Windows
```bash
> gradlew.bat bootRepackage
```


#### How to run
```bash
> java -jar build/libs/build/libs/spring-boot-tornadofx-0.0.1-SNAPSHOT.jar
```

#### Links
- Kotlin https://kotlinlang.org/
- SpringBoot https://projects.spring.io/spring-boot
- Gradle https://gradle.org/

###Docker image
Run gradle task `buildDocker` that will compile frontend and backend

###Liquibase commands
C:\Projects\house-payments>.\gradlew rollbackCount -PliquibaseCommandValue=1 --info
