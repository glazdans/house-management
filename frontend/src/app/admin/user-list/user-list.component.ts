import { Component, OnInit } from '@angular/core';
import {User} from '@app/admin/user'
import {UserService} from '@app/admin/user.service'
import {Building} from '@app/building/building'

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  users: User[];
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getAllUsers()
      .subscribe(users => {
        this.users = users;
      })
  }

  public getBuildingString(buildings: Building[]): string {
    return buildings
      .map((building) => building.address)
      .reduce((prev, current) => prev + ", " + current)
  }

}
