import {CommonModule} from '@angular/common'
import {NgModule} from '@angular/core'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import {UserListComponent} from '@app/admin/user-list/user-list.component'
import {UserRoutingModule} from '@app/admin/user-routing.module'
import {UserService} from '@app/admin/user.service'
import {CoreModule} from '@app/core'
import {SharedModule} from '@app/shared'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap'
import {TranslateModule} from '@ngx-translate/core'
import {CalendarModule} from 'primeng/calendar'

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    UserRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    CalendarModule
  ],
  declarations: [UserListComponent],
  providers: [UserService],
  bootstrap: []
})
export class UserModule { }
