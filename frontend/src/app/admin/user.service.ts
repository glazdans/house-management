import {Injectable} from '@angular/core';
import {User} from '@app/admin/user'
import {HttpService} from '@app/core'
import {Observable} from "rxjs";

const routes = {
  allUsers: () => `/admin/user/`,
};


@Injectable()
export class UserService {

  constructor(private httpClient: HttpService) {
  }

  getAllUsers(): Observable<User[]> {
    return this.httpClient
      .get(routes.allUsers())
      .pipe((body:any) => body);
  }
}
