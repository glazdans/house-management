import {Building} from '@app/building/building'

export class User {
  userId: number;
  email: string;
  buildings: Building[]
}
