import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder} from "@angular/forms";
import {ModalComponent} from "@app/core/ui/modal.component";
import {Apartment, ApartmentService} from "@app/apartment-list/apartment.service";

@Component({
  selector: 'apartment-list-modal',
  template: `
    <div class="modal-header">
      <h4 class="modal-title" translate>Apartment</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form (ngSubmit)="onSubmit()">

      <div class="modal-body" content="h-75">
        <div style="overflow: auto">
          <p-table [value]="apartments" [(selection)]="selectedApartments" dataKey="id">
            <ng-template pTemplate="header">
              <tr>
                <th scope="col" style="width: 3em">
                  <p-tableHeaderCheckbox></p-tableHeaderCheckbox>
                </th>
                <th scope="col" translate>Apartment number</th>
                <th scope="col" translate>Tenant name</th>
              </tr>
            </ng-template>
            <ng-template pTemplate="body" let-rowData let-apartment>
              <tr [pSelectableRow]="rowData">
                <td>
                  <p-tableCheckbox [value]="rowData"></p-tableCheckbox>
                </td>
                <td>{{apartment.apartmentNumber}}</td>
                <td>{{apartment.tenantName}}</td>
              </tr>
            </ng-template>
          </p-table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success" [disabled]="form.invalid" translate>Choose</button>
      </div>
    </form>
  `
})
export class ApartmentListModalComponent extends ModalComponent implements OnInit {
  buildingId: number;
  apartments: Apartment[];
  selectedApartments: Apartment[];

  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder, private apartmentService: ApartmentService) {
    super(activeModal);
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({});
    // TODO add sorting\
    this.apartmentService.getAllAparatments(this.buildingId)
      .subscribe(apartments => this.apartments = apartments);

    this.validateForm();
  }

  public onSubmit(){
    this.callback(this.selectedApartments);
    this.activeModal.close();
    return;
  }

  setValueFields(value: any): any {
  }
}
