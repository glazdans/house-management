import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { Shell } from '@app/shell/shell.service';
import {ApartmentListComponent} from "@app/apartment-list/apartment-list/apartment-list.component";

const routes: Routes = [
  Shell.childRoutes([
    { path: 'apartment-list', component: ApartmentListComponent, data: { title: extract('Apartments') } }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ApartmentListRoutingModule { }
