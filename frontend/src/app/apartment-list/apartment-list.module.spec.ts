import { ApartmentListModule } from './apartment-list.module';

describe('ApartmentListModule', () => {
  let apartmentListModule: ApartmentListModule;

  beforeEach(() => {
    apartmentListModule = new ApartmentListModule();
  });

  it('should create an instance', () => {
    expect(apartmentListModule).toBeTruthy();
  });
});
