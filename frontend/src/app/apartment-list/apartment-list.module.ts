import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ApartmentService} from "@app/apartment-list/apartment.service";
import {ApartmentListComponent} from "@app/apartment-list/apartment-list/apartment-list.component";
import {WaterReadingsModule} from '@app/water-readings/water-readings.module'
import {TranslateModule} from "@ngx-translate/core";
import {CoreModule} from "@app/core";
import {SharedModule} from "@app/shared";
import {Angulartics2Module} from "angulartics2";
import {ApartmentListRoutingModule} from "@app/apartment-list/apartment-list-routing.module";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {ApartmentModalComponent} from "@app/apartment-list/apartment-modal/apartment-modal.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MonthPeriodModule} from "@app/month-period/month-period.module";
import {ApartmentListModalComponent} from "@app/apartment-list/apartment-list-modal/apartment-list-modal";
import {TableModule} from "primeng/table";


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    Angulartics2Module,
    ApartmentListRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MonthPeriodModule,
    TableModule,
    WaterReadingsModule
  ],
  declarations: [ApartmentListComponent, ApartmentModalComponent, ApartmentListModalComponent],
  providers: [ApartmentService],
  bootstrap: [ApartmentModalComponent, ApartmentListModalComponent]
})
export class ApartmentListModule { }
