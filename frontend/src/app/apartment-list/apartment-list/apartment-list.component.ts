import {Component, OnInit} from '@angular/core'
import {BuildingService} from '@app/building/building.service'
import {Apartment, ApartmentDto, ApartmentService} from '../apartment.service'
import {NgbModal} from '@ng-bootstrap/ng-bootstrap'
import {ApartmentModalComponent} from '../apartment-modal/apartment-modal.component'
import {BillService} from '../../bill/bill.service'
import {TranslateService} from '@ngx-translate/core'
import {MessageService} from 'primeng/api'

@Component({
  selector: 'app-apartment-list',
  templateUrl: './apartment-list.component.html',
  styleUrls: ['./apartment-list.component.scss']
})
export class ApartmentListComponent implements OnInit {
  buildingId: number
  apartmentDtos: ApartmentDto[]

  page = 1
  totalItems = 20

  constructor(private apartmentService: ApartmentService,
    private billService: BillService,
    private buildingService: BuildingService,
    private messageService: MessageService,
    private translate: TranslateService,
    private modalService: NgbModal) {
  }

  ngOnInit() {
    this.buildingService.selectedBuilding().subscribe(buildingId => {
      if (buildingId) {
        this.buildingId = buildingId
        this.init()
      }
    })
  }

  private init() {
    this.apartmentService.getAllApartmentCount(this.buildingId)
      .subscribe(count => this.totalItems = count)
    this.loadPage(this.page)
  }

  loadPage(page: number) {
    this.apartmentService.getAllApartments(page, this.buildingId)
      .subscribe((apartments: ApartmentDto[]) => this.apartmentDtos = apartments)
  }

  openApartmentCreate() {
    const modalRef = this.modalService.open(ApartmentModalComponent)
    modalRef.componentInstance.model = new Apartment()
    modalRef.componentInstance.callback = this.saveApartment(this.apartmentService)
  }

  openApartmentEdit(apartment: Apartment) {
    // TODO replace with a new instance, so not to edit existing one
    // https://stackoverflow.com/questions/28150967/typescript-cloning-object
    const modalRef = this.modalService.open(ApartmentModalComponent)
    modalRef.componentInstance.model = {...apartment} // TODO not good for nested properties? (Maybe not a bad thing)?
    modalRef.componentInstance.callback = this.saveApartment(this.apartmentService)
  }

  saveApartment(apartmentService: ApartmentService) {
    return (apartment: Apartment) => {
      this.apartmentService.updateApartment(this.buildingId, apartment).subscribe(() => {
        this.init()
      })
    }
  }

}


