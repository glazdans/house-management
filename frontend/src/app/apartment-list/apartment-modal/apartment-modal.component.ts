import {Component, Input, OnInit} from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import {ModalComponent} from "@app/core/ui/modal.component";

@Component({
  selector: 'apartment-modal',
  templateUrl: './apartment-modal.component.html'
})
export class ApartmentModalComponent extends ModalComponent implements OnInit{
  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder) { super(activeModal);}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      apartmentNumber: [this.model.apartmentNumber, Validators.required],
      size: [this.model.size, Validators.required],
      tenantName: [this.model.tenantName, Validators.required],
      tenantPersonCode: [this.model.tenantPersonCode],
      inhabitantCount: [this.model.inhabitantCount],
    });
    this.validateForm();
  }

  setValueFields(value: any): any {
    value.id = this.model.id;
  }
}
