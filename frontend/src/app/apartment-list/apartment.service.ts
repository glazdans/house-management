import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs/index";
import {catchError, map} from "rxjs/internal/operators";
import {HttpCacheService, HttpService} from '@app/core'

// TODO handle page sizes
const routes = {
  apartment: (buildingId: number) => `/building/${buildingId}/apartment/`,
  allApartmentsPaged: (page: number, buildingId: number) => `/building/${buildingId}/apartment?page=${page}&size=10`,
  count: (buildingId: number) => `/building/${buildingId}/apartment/count`,
};


@Injectable()
export class ApartmentService {

  constructor(private httpClient: HttpService, private cacheService: HttpCacheService) { }

  // TODO split water readings into seperate rest/functionality or even /tab
  public getAllApartments(page: number, buildingId: number): Observable<ApartmentDto[]> {
    return this.httpClient
      // .cache()  // TODO solve issue with updates and getting neweest info/ page numbers
      .get(routes.allApartmentsPaged(page - 1, buildingId)) // TODO handle 1 => 0 conversion more globally
      .pipe(
        map((body: any) => body),
        catchError(() => of('Error, could not load joke :-('))
      );
  }

  // TODO add all apartments
  public getAllAparatments(buildingId: number): Observable<Apartment[]> {
    return this.httpClient
      .get(routes.apartment(buildingId))
      .pipe(
        map((body: any) => body),
        catchError(() => of('Error, could not load joke :-('))
      );
  }

  public getAllApartmentCount(buildingId: number): Observable<number> {
    return this.httpClient
      //.cache()
      .get(routes.count(buildingId))
      .pipe(
        map((body: any) => body),
        catchError(() => of('Error, could not load joke :-('))
      );
  }

  public updateApartment(buildingId:number, apartment: Apartment): Observable<Apartment> {
    return this.httpClient
      .post(routes.apartment(buildingId),apartment)
      .pipe(
        map((body: any) => body),
        catchError((error) => {return of('Error: Update apartment');})
      );
  }

}

export class ApartmentDto {
  apartment: Apartment;
}

export class Apartment {
  id: number;
  apartmentNumber: number;
  tenantName: string;
  tenantPersonCode: string;
  size: number;
  inhabitantCount: number;
}
