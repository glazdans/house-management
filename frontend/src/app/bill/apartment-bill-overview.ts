export class ApartmentBillOverview {
  monthPeriodId: number;
  apartmentId: number;
  apartmentNumber: number;
  tenantName: string;
  billTotal: number;
}
