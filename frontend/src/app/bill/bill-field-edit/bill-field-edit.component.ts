import {Component, Input, OnInit} from '@angular/core'
import {BillField, BillProperty} from '@app/bill/bill-field'
import {BillFieldService} from '@app/bill/bill-field.service'
import {BuildingService} from '@app/building/building.service'
import {NgbModal} from '@ng-bootstrap/ng-bootstrap'
import {AbstractControl, FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms'
import {ApartmentListModalComponent} from '@app/apartment-list/apartment-list-modal/apartment-list-modal'
import {Apartment} from '@app/apartment-list/apartment.service'
import {ActivatedRoute, Router} from '@angular/router'

@Component({
  selector: 'bill-field-edit',
  templateUrl: './bill-field-edit.component.html'
})
export class BillFieldEditComponent implements OnInit {
  id: number
  buildingId: number
  propertyList: string[]
  form: FormGroup
  originalBillField: BillField
  apartment: Apartment[]

  constructor(
    private buildingService: BuildingService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private billFieldService: BillFieldService) {
  }

  ngOnInit(): void {
    this.buildingService.selectedBuilding().subscribe(buildingId => {
      if(buildingId) {
        this.buildingId = buildingId
      }
    })

    this.id = +this.route.snapshot.paramMap.get('id')
    let billField = new BillField()
    billField.apartment = []
    billField.defaultField = true
    billField.validFrom = new Date()
    this.createForm(billField)
    if (this.id) {
      this.billFieldService.getBillField(this.buildingId, this.id).subscribe(billField => this.createForm(billField))
    }

    this.propertyList = Object.keys(BillProperty)
      .filter(k => typeof BillProperty[k] === 'number') as string[]
  }

  createForm(billField: BillField) {
    this.form = this.formBuilder.group({
      name: [billField.name, Validators.required],
      tariff: [billField.tariff, Validators.required],
      property: [billField.property, Validators.required],
      validFrom: [billField.validFrom],
      validUntil: [billField.validUntil],
      footerNote: [billField.footerNote, Validators.maxLength(255)],
      defaultField: [billField.defaultField],
    })
    this.originalBillField = billField
    this.apartment = billField.apartment
  }

  public onSubmit() {
    if (!this.form.valid) {
      return
    }
    let value = this.form.value
    value.apartment = this.apartment

    Object.keys(value).forEach(key => this.originalBillField[key] = value[key])
    if(this.id) {
      this.billFieldService.updateBillField(this.buildingId, this.originalBillField)
        .subscribe(() => {
            this.router.navigate(['/bill-field-list/'])
          }
        )
    } else {
      this.billFieldService.addBillField(this.buildingId, this.originalBillField)
        .subscribe(() => {
            this.router.navigate(['/bill-field-list/'])
          }
        )
    }

  }

  openApartmentList() {
    const modalRef = this.modalService.open(ApartmentListModalComponent, {size: 'lg'})
    modalRef.componentInstance.buildingId = this.buildingId
    modalRef.componentInstance.selectedApartments = [...this.apartment]
    modalRef.componentInstance.callback = (apartments: Apartment[]) => {
      this.apartment = apartments
    }
  }

  public canShowError(control: AbstractControl) {
    return control.errors && (control.dirty || control.touched)
  }

  // convenience getter for easy access to form fields
  public get f() {
    return this.form.controls
  }
}
