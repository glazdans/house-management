import {Component, OnInit} from '@angular/core'
import {BillField} from '@app/bill/bill-field'
import {BillFieldService} from '@app/bill/bill-field.service'
import {ActivatedRoute, Router} from '@angular/router'
import {BuildingService} from '@app/building/building.service'

@Component({
  selector: 'app-bill-field-list',
  templateUrl: './bill-field-list.component.html',
  styleUrls: ['./bill-field-list.component.scss']
})
export class BillFieldListComponent implements OnInit {
  buildingId: number
  billFields: BillField[]

  constructor(
    private buildingService: BuildingService,
    private billFieldService: BillFieldService,
    private router: Router) {
  }

  ngOnInit() {
    this.buildingService.selectedBuilding().subscribe(buildingId => {
      if (buildingId) {
        this.buildingId = buildingId
        this.refreshTable()
      }
    })
  }

  refreshTable() {
    if (this.buildingId) {
      this.billFieldService.getBillFields(this.buildingId)
        .subscribe(billFields => {
          this.billFields = billFields
        })
    }
  }

  createBillField() {
    this.router.navigate(['/bill-field/', this.buildingId,])
  }

  openBillField(billField: BillField) {
    // this.router.navigate(['/bill-field/', {id: billField.id}]);
    this.router.navigate(['/bill-field/', this.buildingId, billField.id])
  }

  deleteBillField(billField: BillField) {
    this.billFieldService.deleteBillField(this.buildingId, billField)
      .subscribe(() => this.refreshTable())
  }

}


