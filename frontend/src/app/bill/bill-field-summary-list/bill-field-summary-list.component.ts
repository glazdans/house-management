import {Component, OnInit} from '@angular/core'
import {BillFieldSummary} from '@app/bill/bill-field-summary'
import {ActivatedRoute} from '@angular/router'
import {BillService} from '@app/bill/bill.service'
import {BuildingService} from '@app/building/building.service'

@Component({
  selector: 'app-bill-field-summary-list',
  templateUrl: './bill-field-summary-list.component.html'
})
export class BillFieldSummaryListComponent implements OnInit {
  buildingId: number
  monthPeriodId: number
  billFieldSummary: BillFieldSummary[]

  constructor(
    private buildingService: BuildingService,
    private billService: BillService,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.monthPeriodId = +this.route.snapshot.paramMap.get('monthPeriodId')
    this.buildingService.selectedBuilding().subscribe(buildingId => {
      if (buildingId) {
        this.buildingId = buildingId
        this.refreshTable()
      }
    })
  }

  refreshTable() {
    this.billService.getBillFieldSummary(this.buildingId, this.monthPeriodId)
      .subscribe(billFieldSummary => this.billFieldSummary = billFieldSummary)
  }

}


