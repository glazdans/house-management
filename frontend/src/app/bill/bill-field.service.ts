import {Injectable} from '@angular/core'
import {BillField} from '@app/bill/bill-field'
import {HttpCacheService, HttpService} from '@app/core'
import {BillNotification} from '@app/month-period/month-period.service'
import {MonthYear} from '@app/month-period/month-year'
import {Observable, of} from 'rxjs'
import {catchError, map} from 'rxjs/operators'

const routes = {
  billField: (buildingId: number, billFieldId: number) => `/building/${buildingId}/bill-field/${billFieldId}`,
  newBillField: (buildingId: number) => {
    return `/building/${buildingId}/bill-field`
  },
  billFieldEdit: (buildingId: number, billFieldId: number) => {
    return `/building/${buildingId}/bill-field/${billFieldId}`
  },
  billFields: (buildingId: number) => `/building/${buildingId}/bill-field/`
}

@Injectable()
export class BillFieldService {

  constructor(private httpClient: HttpService, private cacheService: HttpCacheService) {
  }

  getBillFields(buildingId: number): Observable<BillField[]> {
    return this.httpClient
      .get(routes.billFields(buildingId))
      .pipe(map((body: BillField[]) => body.map(this.mapDate)))
      .pipe(
        map((body: any) => body),
        catchError(() => of('Error, could not load joke :-('))
      )
  }

  getBillField(buildingId: number, billFieldId: number): Observable<BillField> {
    return this.httpClient
      .get(routes.billField(buildingId, billFieldId))
      .pipe(map(this.mapDate))
      .pipe(
        map((body: any) => body),
        catchError(() => of('Error, could not load joke :-('))
      )
  }

  addBillField(buildingId: number, billField: BillField): Observable<BillField> {
    return this.httpClient
      .post(routes.newBillField(buildingId), billField)
      .pipe(map(this.mapDate))
      .pipe(
        map((body: any) => body),
        catchError(() => of('Error, could not load joke :-('))
      )
  }

  updateBillField(buildingId: number, billField: BillField): Observable<BillField> {
    return this.httpClient
      .put(routes.billFieldEdit(buildingId, billField.id), billField)
      .pipe(map(this.mapDate))
      .pipe(
        map((body: any) => body),
        catchError(() => of('Error, could not load joke :-('))
      )
  }

  deleteBillField(buildingId: number, billField: BillField): Observable<BillField> {
    return this.httpClient
      .request('delete', routes.billField(buildingId, billField.id), {body: billField})
      .pipe(
        map((body: any) => body),
        catchError(() => of('Error, could not load joke :-('))
      )
  }

  private mapDate(billField: BillField): BillField {
    if(billField.validFrom) {
      billField.validFrom = new Date(billField.validFrom)
    }
    if (billField.validUntil) {
      billField.validUntil = new Date(billField.validUntil)
    }
    return billField
  }

}
