import {Apartment} from '@app/apartment-list/apartment.service'
import {MonthPeriod} from '@app/month-period/month-period.service'

export class BillField {
  name: string;
  tariff: number;
  property: BillProperty;
  defaultField: boolean;
  monthPeriod: MonthPeriod;
  apartment: Apartment[];
  validFrom: Date;
  validUntil: Date;
  footerNote: string;
  deleted: number;
  id: number;
}

export enum BillProperty {
  APARTMENT, SIZE, HOT_WATER, COLD_WATER, ALL_WATER, INHABITANTS
}
