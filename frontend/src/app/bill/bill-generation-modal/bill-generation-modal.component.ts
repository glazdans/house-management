import {Component, OnInit} from '@angular/core'
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap'
import {FormBuilder, Validators} from '@angular/forms'
import {ModalComponent} from '@app/core/ui/modal.component'

@Component({
  selector: 'bill-generation-modal',
  templateUrl: './bill-generation-modal.component.html'
})
export class BillGenerationModalComponent extends ModalComponent implements OnInit {
  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
  ) {
    super(activeModal)
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      billNotification: [""],
      billDate: [null, Validators.required]
    })
  }

  setValueFields(value: any): any {
  }

}
