import {Component, OnInit} from '@angular/core'
import {ApartmentBillOverview} from '@app/bill/apartment-bill-overview'
import {ActivatedRoute, Router} from '@angular/router'
import {NgbModal} from '@ng-bootstrap/ng-bootstrap'
import { BillService } from '../bill.service'

@Component({
  selector: 'app-bill-list-apartments',
  templateUrl: './bill-list-apartments.component.html'
})
export class BillListApartmentsComponent implements OnInit {
  buildingId: number
  monthPeriodId: number
  apartmentBillOverviews: ApartmentBillOverview[]

  constructor(
    private billService: BillService,
    private router: Router,
    private modalService: NgbModal,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.buildingId = +this.route.snapshot.paramMap.get('buildingId')
    this.monthPeriodId = +this.route.snapshot.paramMap.get('monthPeriodId')
    this.billService.getApartmentBillOverview(this.buildingId, this.monthPeriodId)
      .subscribe(dtos => {
        this.apartmentBillOverviews = dtos
      })
  }

  downloadBill(overviewDto: ApartmentBillOverview) {
      this.billService.getBill(this.buildingId, overviewDto.monthPeriodId, overviewDto.apartmentId)
        .subscribe(file => this.download(file, `${overviewDto.apartmentNumber}.docx`))
  }

  private download(fileUrl: any, fileName: string) {
    var fileLink = document.createElement('a')
    fileLink.href = fileUrl
    fileLink.download = fileName
    fileLink.click()
  }
}


