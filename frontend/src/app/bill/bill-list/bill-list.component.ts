import {Component, OnInit} from '@angular/core'
import {BillGenerationModalComponent} from '@app/bill/bill-generation-modal/bill-generation-modal.component'
import {BillOverviewDto, BillStatus} from '@app/bill/bill-overview-dto'
import {ActivatedRoute, Router} from '@angular/router'
import {BuildingService} from '@app/building/building.service'
import {NgbModal} from '@ng-bootstrap/ng-bootstrap'
import {BillService} from '../bill.service'

@Component({
  selector: 'app-bill-list',
  templateUrl: './bill-list.component.html'
})
export class BillListComponent implements OnInit {
  buildingId: number
  billOverviewDtos: BillOverviewDto[]
  BillStatus = BillStatus

  constructor(
    private buildingService: BuildingService,
    private billService: BillService,
    private router: Router,
    private modalService: NgbModal) {
  }

  ngOnInit() {
    this.buildingService.selectedBuilding().subscribe(buildingId => {
      if (buildingId) {
        this.buildingId = buildingId
        this.refreshTable()
      }
    })
  }

  refreshTable() {
    this.billService.getBillOverviews(this.buildingId)
      .subscribe(dtos => {
        this.billOverviewDtos = dtos.sort(this.sortDtos).reverse()
      })
  }

  private sortDtos(a: BillOverviewDto, b: BillOverviewDto): number {
    const yearDiff = a.year - b.year
    if (yearDiff != 0) {
      return yearDiff
    }
    return a.month - b.month
  }

  openBillFieldSummary(overviewDto: BillOverviewDto) {
    this.router.navigate(['/bill-field-summary/', this.buildingId, overviewDto.monthPeriodId])
    /*{
      buildingId: this.buildingId,
      year: this.selectedMonthYear.year,
      month: this.selectedMonthYear.month
    }])*/
  }

  openApartmentView(overviewDto: BillOverviewDto) {
    this.router.navigate(['/bill-list-apartment/', this.buildingId, overviewDto.monthPeriodId])
    /*{
      buildingId: this.buildingId,
      year: this.selectedMonthYear.year,
      month: this.selectedMonthYear.month
    }])*/
  }

  downloadBillsForMonth(overviewDto: BillOverviewDto) {
    this.billService.getAllBills(this.buildingId, overviewDto.monthPeriodId)
      .subscribe(url => this.download(url, `bills_${overviewDto.year}_${overviewDto.month}.zip`))
  }

  deleteBillForMonth(overviewDto: BillOverviewDto) {
    this.billService.deleteBill(this.buildingId, overviewDto.monthPeriodId)
      .subscribe(() => this.refreshTable())
  }

  private download(fileUrl: any, fileName: string) {
    var fileLink = document.createElement('a')
    fileLink.href = fileUrl
    fileLink.download = fileName
    fileLink.click()
  }

  openCreateBillModal() {
    const modalRef = this.modalService.open(BillGenerationModalComponent, {size: 'lg'})
    modalRef.componentInstance.model = {}
    modalRef.componentInstance.callback = (request: { billNotification: string, billDate: Date }) => {
      this.billService.generateNewBill(this.buildingId, request)
        .subscribe((dto) => {
          this.refreshTable()
        })
    }
  }
}


