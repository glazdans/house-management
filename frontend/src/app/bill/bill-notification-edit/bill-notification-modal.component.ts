import {Component, OnInit} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder} from "@angular/forms";
import {MONTH_NAMES, MonthPeriod} from "@app/month-period/month-period.service";
import {ModalComponent} from "@app/core/ui/modal.component";

@Component({
  selector: 'bill-notification-edit',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">{{'Bill notification' | translate}} - {{getMonthName(monthPeriod.month) | translate}}, {{monthPeriod.year}}</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form [formGroup]="form" (ngSubmit)="onSubmit()" >
        <div class="form-group">
          <label for="notification" translate>Bill notification</label>\
          <textarea id="notification" rows="6" formControlName="notification" class="form-control" name="notification" [ngClass]="{ 'invalid': canShowError(f.notification)}"></textarea>
        </div>
 
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" [disabled]="form.invalid" translate> Submit</button>
          <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
        </div>
      </form>
    </div>
  `
})
export class BillNotificationModalComponent extends ModalComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder) {
    super(activeModal)
  }

  monthPeriod: MonthPeriod;

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      notification: [this.model.notification],
    });
  }

  setValueFields(value: any): any {
  }

  getMonthName(month: number): string {
    return MONTH_NAMES[month];
  }
}
