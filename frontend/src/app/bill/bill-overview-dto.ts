export class BillOverviewDto {
  monthPeriodId: number
  month: number
  year: number
  billDate: Date
  allBillTotal: number
  billStatus: BillStatus
}

export enum BillStatus {
  NO_BILL, ACTIVE, OLD, DELETED
}
