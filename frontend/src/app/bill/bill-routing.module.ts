import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BillFieldSummaryListComponent} from '@app/bill/bill-field-summary-list/bill-field-summary-list.component'
import {BillListApartmentsComponent} from '@app/bill/bill-list-apartments/bill-list-apartments.component'
import {BillListComponent} from '@app/bill/bill-list/bill-list.component'

import { extract } from '@app/core';
import { Shell } from '@app/shell/shell.service';
import {BillFieldListComponent} from "@app/bill/bill-field-list/bill-field-list.component";
import {BillFieldEditComponent} from "@app/bill/bill-field-edit/bill-field-edit.component";

const routes: Routes = [
  Shell.childRoutes([
    { path: 'bill-list', component: BillListComponent, data: { title: extract('Bills') } },
    { path: 'bill-list-apartment/:buildingId/:monthPeriodId', component: BillListApartmentsComponent, data: { title: extract('Bills apartment view') } },
    { path: 'bill-field-summary/:buildingId/:monthPeriodId', component: BillFieldSummaryListComponent, data: { title: extract('Bill field summary') } },
    { path: 'bill-field-list', component: BillFieldListComponent, data: { title: extract('Bills') } },

    { path: 'bill-field/:buildingId', component: BillFieldEditComponent, data: { title: extract('Bill field') } },
    { path: 'bill-field/:buildingId/:id', component: BillFieldEditComponent, data: { title: extract('Bill field') } }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillRoutingModule { }
