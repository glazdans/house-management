import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {BillFieldSummaryListComponent} from '@app/bill/bill-field-summary-list/bill-field-summary-list.component'
import {BillFieldService} from '@app/bill/bill-field.service'
import {BillGenerationModalComponent} from '@app/bill/bill-generation-modal/bill-generation-modal.component'
import {BillListApartmentsComponent} from '@app/bill/bill-list-apartments/bill-list-apartments.component'
import {BillListComponent} from '@app/bill/bill-list/bill-list.component'
import {TranslateModule} from '@ngx-translate/core'
import {CoreModule} from '@app/core'
import {SharedModule} from '@app/shared'
import {Angulartics2Module} from 'angulartics2'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {BillFieldListComponent} from '@app/bill/bill-field-list/bill-field-list.component'
import {BillService} from '@app/bill/bill.service'
import {BillRoutingModule} from '@app/bill/bill-routing.module'
import {MonthPeriodModule} from '@app/month-period/month-period.module'
import {BillFieldEditComponent} from '@app/bill/bill-field-edit/bill-field-edit.component'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import {BillNotificationModalComponent} from '@app/bill/bill-notification-edit/bill-notification-modal.component'
import {CalendarModule} from 'primeng/calendar'

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    Angulartics2Module,
    BillRoutingModule,
    NgbModule,
    FormsModule,
    MonthPeriodModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    CalendarModule
  ],
  declarations: [BillFieldListComponent, BillFieldEditComponent, BillNotificationModalComponent, BillListComponent,
    BillGenerationModalComponent, BillListApartmentsComponent, BillFieldSummaryListComponent],
  providers: [BillService, BillFieldService],
  bootstrap: [BillNotificationModalComponent, BillGenerationModalComponent]
})
export class BillModule {
}
