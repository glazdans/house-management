import {Injectable} from '@angular/core'
import {HttpHeaders} from '@angular/common/http'
import {ApartmentBillOverview} from '@app/bill/apartment-bill-overview'
import {BillFieldSummary} from '@app/bill/bill-field-summary'
import {BillOverviewDto} from '@app/bill/bill-overview-dto'
import {HttpCacheService, HttpService} from '@app/core'
import {MonthYear} from '@app/month-period/month-year'
import {of} from 'rxjs'
import {Observable} from 'rxjs/index'
import {catchError, map} from 'rxjs/operators'

const routes = {
  bill: (buildingId: number, monthPeriodId: number, apartmentId: number) => {
    return `/building/${buildingId}/bill/${monthPeriodId}/apartment/${apartmentId}`
  },
  generateBill: (buildingId: number) => {
    return `/building/${buildingId}/bill`
  },
  billError: (buildingId: number, monthYear: MonthYear, apartmentId: number) => {
    return `/building/${buildingId}/bill/${monthYear.month}/${monthYear.year}/apartment/${apartmentId}/error`
  },
  allBills: (buildingId: number, monthPeriodId: number) => {
    return `/building/${buildingId}/bill/${monthPeriodId}`
  },
  billOverviews: (buildingId: number) => `/building/${buildingId}/bill-overview`,
  apartmentBillOverviews: (buildingId: number, monthPeriodId: number) => {
    return `/building/${buildingId}/bill-overview/${monthPeriodId}`
  },
  billFieldSummary: (buildingId: number, monthPeriodId: number) => {
    return `/building/${buildingId}/bill-overview/${monthPeriodId}/bill-field-summary`
  }
}

@Injectable()
export class BillService {

  constructor(private httpClient: HttpService, private cacheService: HttpCacheService) {
  }

  getBillOverviews(buildingId: number): Observable<BillOverviewDto[]> {
    return this.httpClient
      .get(routes.billOverviews(buildingId))
      .pipe(map((body: BillOverviewDto[]) => body.map(this.mapDate)))
      .pipe(
        map((body: any) => body)
      )
  }

  getApartmentBillOverview(buildingId: number, monthPeriodId: number): Observable<ApartmentBillOverview[]> {
    return this.httpClient
      .get(routes.apartmentBillOverviews(buildingId, monthPeriodId))
      .pipe(
        map((body: any) => body)
      )
  }

  getBillFieldSummary(buildingId: number, monthPeriodId: number): Observable<BillFieldSummary[]> {
    return this.httpClient
      .get(routes.billFieldSummary(buildingId, monthPeriodId))
      .pipe(
        map((body: any) => body )
      )
  }

  generateNewBill(buildingId: number, request: any): Observable<BillOverviewDto> {
    return this.httpClient
      .post(routes.generateBill(buildingId), request)
      .pipe(map((this.mapDate)))
      .pipe(
        map((body: any) => body)
      )
  }

  getBill(buildingId: number, monthPeriodId: number, apartmentId: number) {
    let headers = new HttpHeaders()
    headers = headers.set('Accept', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
    return this.httpClient
      .get(routes.bill(buildingId, monthPeriodId, apartmentId), {headers: headers, responseType: 'blob'})
      .pipe(
        map((body: any) => URL.createObjectURL(body))
      )
  }

  getBillError(buildingId: number, monthYear: MonthYear, apartmentId: number) {
    let headers = new HttpHeaders()
    return this.httpClient
      .get(routes.billError(buildingId, monthYear, apartmentId), {headers: headers})
      .pipe(
        map((body: any) => body)
      )
  }

  getAllBills(buildingId: number, monthPeriodId: number) {
    let headers = new HttpHeaders()
    headers = headers.set('Accept', 'application/zip')
    return this.httpClient
      .get(routes.allBills(buildingId, monthPeriodId), {headers: headers, responseType: 'blob'})
      .pipe(
        map((body: any) => URL.createObjectURL(body)),
        catchError(() => of('Error, could not load joke :-('))
      )
  }

  deleteBill(buildingId: number, monthPeriodId: number): Observable<void> {
    return this.httpClient
      .delete(routes.allBills(buildingId, monthPeriodId))
      .pipe(map((body: any) => body))
  }

  private mapDate(dto: BillOverviewDto) {
    if (dto.billDate) {
      dto.billDate = new Date(dto.billDate)
    }
    return dto
  }
}


