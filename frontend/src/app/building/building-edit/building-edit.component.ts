import {Component, OnInit} from '@angular/core'
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms'
import {ActivatedRoute, Router} from '@angular/router'
import {Building} from '@app/building/building'
import {BuildingService} from '@app/building/building.service'
import {Observable} from 'rxjs/index'

@Component({
  selector: 'app-building-edit',
  templateUrl: './building-edit.component.html',
  styleUrls: ['./building-edit.component.scss']
})
export class BuildingEditComponent implements OnInit {
  buildingId: number
  form: FormGroup

  constructor(private route: ActivatedRoute,
    private router: Router,
    private buildingService: BuildingService,
    private formBuilder: FormBuilder,) {
  }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id')
    if(id) {
      this.buildingId = id
      this.buildingService.getBuilding(id)
        .subscribe(building => this.createForm(building))
    } else {
      this.createForm(new Building())
    }
  }

  createForm(building: Building) {
    this.form = this.formBuilder.group({
      address: [building.address, Validators.required],
      landlord: [building.landlord, Validators.required],
      landlordAddress: [building.landlordAddress, Validators.required],
      regNr: [building.regNr, Validators.required],
      bank: [building.bank, Validators.required],
      swift: [building.swift, Validators.required],
      bankAccount: [building.bankAccount, Validators.required],
    })
  }

  public onSubmit() {
    if (!this.form.valid) {
      return
    }
    let value = this.form.value

    let saveBuilding: Observable<Building>
    if (this.buildingId) {
      saveBuilding = this.buildingService.updateBuilding(this.buildingId, value)
    } else {
      saveBuilding = this.buildingService.addBuilding(value)
    }
    saveBuilding.subscribe(() => {
        this.router.navigate(['/building-list'])
      }
    )
  }

  public canShowError(control: AbstractControl){
    return control.errors && (control.dirty || control.touched)
  }

  // convenience getter for easy access to form fields
  public get f() { return this.form.controls; }

}
