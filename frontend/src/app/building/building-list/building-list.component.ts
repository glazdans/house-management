import {Component, OnInit} from '@angular/core'
import {Router} from '@angular/router'
import {Building} from '@app/building/building'
import {BuildingService} from '@app/building/building.service'

@Component({
  selector: 'app-building-list',
  templateUrl: './building-list.component.html',
  styleUrls: ['./building-list.component.scss']
})
export class BuildingListComponent implements OnInit {
  buildings: Building[]

  constructor(private buildingService: BuildingService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.buildingService.getUserBuildings()
      .subscribe(buildings => {
        this.buildings = buildings
      })
  }

  createBuilding() {
    this.router.navigate(['/building-edit'])
  }

  openBuildingEdit(building: Building) {
    this.router.navigate(['/building-edit/' + building.id])
  }

}
