import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BuildingEditComponent} from '@app/building/building-edit/building-edit.component'
import {BuildingListComponent} from '@app/building/building-list/building-list.component'

import { extract } from '@app/core';
import { Shell } from '@app/shell/shell.service';

const routes: Routes = [
  Shell.childRoutes([
    { path: 'building-list', component: BuildingListComponent, data: { title: extract('Building list') } },
    { path: 'building-edit', component: BuildingEditComponent, data: { title: extract('Building edit') } },
    { path: 'building-edit/:id', component: BuildingEditComponent, data: { title: extract('Building edit') } },
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuildingRouteModule { }
