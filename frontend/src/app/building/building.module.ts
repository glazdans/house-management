import {CommonModule} from '@angular/common'
import {NgModule} from '@angular/core'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import {BuildingEditComponent} from '@app/building/building-edit/building-edit.component'
import {BuildingListComponent} from '@app/building/building-list/building-list.component'
import {BuildingRouteModule} from '@app/building/building-route.module'
import {BuildingService} from '@app/building/building.service'
import {CoreModule} from '@app/core'
import {SharedModule} from '@app/shared'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap'
import {TranslateModule} from '@ngx-translate/core'
import {CalendarModule} from 'primeng/calendar'

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    BuildingRouteModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    CalendarModule
  ],
  declarations: [BuildingListComponent, BuildingEditComponent],
  providers: [BuildingService],
  exports: [],
  bootstrap: []
})
export class BuildingModule { }
