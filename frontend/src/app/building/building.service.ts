import {Injectable} from '@angular/core'
import {Building} from '@app/building/building'
import {HttpService} from '@app/core'
import {BehaviorSubject, Observable, Subject} from 'rxjs'
import {finalize, map} from 'rxjs/operators'

const routes = {
  getUserBuildings: () => `/building/`,
  addBuilding: () => `/building/`,
  updateBuilding: (buildingId: number) => `/building/${buildingId}`,
  getBuilding: (buildingId: number) => `/building/${buildingId}`,
}

@Injectable()
export class BuildingService {
  private _selectedBuildingId: number
  private _selectedBuildingSubject = new BehaviorSubject<number>(null)
  private _userBuildings = new BehaviorSubject<Building[]>([])

  constructor(private httpClient: HttpService) {
  }

  getUserBuildings(): Observable<Building[]> {
    if (this._userBuildings.value.length == 0) {
      this.refreshBuildingsFromDb()
    }
    return this._userBuildings
  }

  private refreshBuildingsFromDb() {
    return this.httpClient
      .get(routes.getUserBuildings())
      .subscribe((buildings: Building[]) => {
        this._userBuildings.next(buildings)
      })
  }

  getBuilding(buildingId: number): Observable<Building> {
    return this.httpClient
      .get(routes.getBuilding(buildingId))
      .pipe(
        map((body: any) => body)
      )
  }

  addBuilding(building: Building): Observable<Building> {
    return this.httpClient
      .post(routes.addBuilding(), building)
      .pipe(
        (body: any) => body,
        finalize(() => this.refreshBuildingsFromDb())
      )

  }

  updateBuilding(buildingId: number, building: Building): Observable<Building> {
    return this.httpClient
      .put(routes.updateBuilding(buildingId), building)
      .pipe(
        (body: any) => body,
        finalize(() => this.refreshBuildingsFromDb())
      )
  }

  selectedBuilding(): Observable<number> {
    return this._selectedBuildingSubject
  }

  set selectedBuildingId(value: number) {
    this._selectedBuildingId = value
    this._selectedBuildingSubject.next(value)
  }
}
