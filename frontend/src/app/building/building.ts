export class Building {
  id: number;
  address: string;
  landlord: string;
  landlordAddress: string;
  regNr: string;
  bank: string;
  swift: string;
  bankAccount: string;
}
