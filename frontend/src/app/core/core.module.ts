import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { RouteReuseStrategy, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { RouteReusableStrategy } from './route-reusable-strategy';
import { AuthenticationService } from './authentication/authentication.service';
import { AuthenticationGuard } from './authentication/authentication.guard';
import { I18nService } from './i18n.service';
import { HttpService } from './http/http.service';
import { HttpCacheService } from './http/http-cache.service';
import { ApiPrefixInterceptor } from './http/api-prefix.interceptor';
import { ErrorHandlerInterceptor } from './http/error-handler.interceptor';
import { CacheInterceptor } from './http/cache.interceptor';
import {NumericPostfixPipe} from "@app/core/numeric-postfix.pipe";
import {TokenInterceptor} from "@app/core/http/token.interceptor";
import {MessageService} from "primeng/api";
import {SpinnerInterceptor} from "@app/core/http/spinner.interceptor";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    TranslateModule,
    RouterModule
  ],
  providers: [
    ApiPrefixInterceptor,
    TokenInterceptor,
    SpinnerInterceptor,
    MessageService,
    ErrorHandlerInterceptor,
    CacheInterceptor,
    {
      provide: HttpClient,
      useClass: HttpService
    },
    AuthenticationService,
    AuthenticationGuard,
    I18nService,
    HttpService,
    HttpCacheService,
    {
      provide: RouteReuseStrategy,
      useClass: RouteReusableStrategy
    },
  ],
  declarations: [NumericPostfixPipe],
  exports: [
    NumericPostfixPipe
  ]
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    // Import guard
    if (parentModule) {
      throw new Error(`${parentModule} has already been loaded. Import Core module in the AppModule only.`);
    }
  }

}
