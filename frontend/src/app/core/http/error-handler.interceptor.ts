import { Injectable } from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '@env/environment';
import { Logger } from '../logger.service';
import {Router} from "@angular/router";
import {MessageService} from "primeng/api";
import {TranslateService} from "@ngx-translate/core";

const log = new Logger('ErrorHandlerInterceptor');

/**
 * Adds a default error handler to all requests.
 */
@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {

  constructor(private router: Router, private messageService: MessageService, private translate: TranslateService){
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError((error: any, caught: Observable<HttpEvent<any>>) => {

      if (!environment.production) {
        // Do something with the error
        log.error('Request error', error);
      }

      if(error instanceof HttpErrorResponse) {
        let res = error as HttpErrorResponse;
        if(res && res.status == 401) {
          this.router.navigate(['/login']);
          return of(error)
        } else {
          this.messageService.add({severity: 'error', summary: this.translate.instant('Error'), detail: res.message})
        }
      }

      throw error;
    }) as any);
  }

}
