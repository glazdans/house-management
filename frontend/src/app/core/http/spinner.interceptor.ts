import {Injectable, Injector} from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

import {AuthenticationService} from "@app/core";
import {NgxSpinnerService} from "ngx-spinner";
import {finalize, tap} from "rxjs/operators";

/**
 * Adds user token to the request
 */
@Injectable()
export class SpinnerInterceptor implements HttpInterceptor {
  count = 0;

  constructor(private spinner: NgxSpinnerService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this.spinner.show();
    this.count++;
    return next.handle(request)
      .pipe(finalize(() => {
        this.count--;
        if(this.count == 0) {
          this.spinner.hide()
        }
      }));
  }

}
