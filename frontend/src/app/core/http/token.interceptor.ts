import {Injectable, Injector} from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

import {AuthenticationService} from "@app/core";

/**
 * Adds user token to the request
 */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private injector: Injector) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authService = this.injector.get(AuthenticationService);
    if(authService.isAuthenticated()){
      const creds = authService.credentials;
      request = request.clone( {
        setHeaders: {
          'Authorization': `Bearer ${creds.token}`
        }
      });
    }
    return next.handle(request);
  }

}
