import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: 'numericPostfix'})
export class NumericPostfixPipe implements PipeTransform {
  transform(value: string, unit: string, power: string): string {
    return `${value} ${unit}<sup>${power}</sup>`;
  }
}
