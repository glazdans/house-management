import { Input, Directive } from "@angular/core";
import {AbstractControl, FormControl, FormGroup} from "@angular/forms";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Directive()
export abstract class ModalComponent {
  form: FormGroup;
  @Input() model: any;
  callback: (x:any)=>void;

  protected constructor(public activeModal: NgbActiveModal) {
  }

  // convenience getter for easy access to form fields
  public get f() { return this.form.controls; }

  public onSubmit(){
    if(!this.form.valid) {
      return;
    }
    let value = this.form.value;
    this.setValueFields(value);
    this.callback(value);
    this.activeModal.close();
  }

  abstract setValueFields(value: any): any;

  protected validateForm(){
    for (let controlKey in this.form.controls) {
      let control = this.form.controls[controlKey];
      control.updateValueAndValidity();
    }
  }

  public canShowError(control: AbstractControl){
    return control.errors && (control.dirty || control.touched)
  }


}
