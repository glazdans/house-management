import {Component, EventEmitter, Output} from '@angular/core';
import {MONTH_NAMES} from '@app/month-period/month-period.service'
import {MonthYear} from '@app/month-period/month-year'

@Component({
  selector: 'app-month-period',
  templateUrl: './month-period.component.html'
})
export class MonthPeriodComponent {
  selectedMonthYear: any;

  @Output() monthYearChange = new EventEmitter<MonthYear>();

  constructor() {
  }

  ngOnInit() {
    this.selectedMonthYear = new Date()
    this.onMonthPeriodChange()
   }

  onMonthPeriodChange() {
    let monthYear = new MonthYear(this.selectedMonthYear.getMonth(), this.selectedMonthYear.getFullYear());
    this.monthYearChange.emit(monthYear);
  }

  private getMonthName(month: number): string {
    return MONTH_NAMES[month];
  }
}
