import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule} from "@ngx-translate/core";
import {CoreModule} from "@app/core";
import {SharedModule} from "@app/shared";
import {Angulartics2Module} from "angulartics2";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule} from "@angular/forms";
import {MonthPeriodComponent} from "@app/month-period/month-period.component";
import {CalendarModule} from 'primeng/calendar'


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    Angulartics2Module,
    NgbModule,
    CalendarModule,
    FormsModule
  ],
  declarations: [MonthPeriodComponent],
  providers: [],
  bootstrap: [],
  exports: [MonthPeriodComponent]
})
export class MonthPeriodModule { }
