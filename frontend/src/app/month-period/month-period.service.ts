export const MONTH_NAMES = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

export class MonthPeriod {
  id: number;
  month: number;
  year: number;
  billNotification: BillNotification;
}

export class BillNotification {
  id: number;
  notification: string;
}
