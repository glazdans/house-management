import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {BuildingModule} from '@app/building/building.module'
import {CoreModule} from '@app/core'
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ShellComponent } from './shell.component';
import { HeaderComponent } from './header/header.component';
import {ToastModule} from "primeng/toast";
import {NgxSpinnerModule} from "ngx-spinner";

@NgModule({
  imports: [
    CoreModule,
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    NgbModule,
    RouterModule,
    ToastModule,
    NgxSpinnerModule,
    BuildingModule,
    FormsModule,
  ],
  declarations: [
    HeaderComponent,
    ShellComponent
  ]
})
export class ShellModule {
}
