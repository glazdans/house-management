export class WaterReadingsDto {
  apartmentId: number;
  apartmentNumber: number;
  tenantName: string;
  hotWater: number;
  coldWater: number;
}
