import {Component, OnInit} from '@angular/core'
import {ActivatedRoute, Router} from '@angular/router'
import {BuildingService} from '@app/building/building.service'
import {MonthYear} from '@app/month-period/month-year'
import {WaterReadingsDto} from '@app/water-readings/water-readings-dto'
import {WaterReadingsModalComponent} from '@app/water-readings/water-readings-modal/water-readings-modal.component'
import {WaterReadingsService} from '@app/water-readings/water-readings.service'
import {NgbModal} from '@ng-bootstrap/ng-bootstrap'

@Component({
  selector: 'app-water-readings-list',
  templateUrl: './water-readings-list.component.html'
})
export class WaterReadingsListComponent implements OnInit {
  buildingId: number
  monthYear: MonthYear
  waterReadingsDtos: WaterReadingsDto[]

  constructor(
    private buildingService: BuildingService,
    private waterReadingsService: WaterReadingsService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.buildingService.selectedBuilding().subscribe(buildingId => {
      if (buildingId) {
        this.buildingId = buildingId
        if(this.monthYear) {
          this.refreshTable(this.monthYear)
        }
      }
    })
  }

  onMonthYearChange(monthYear: MonthYear) {
    this.monthYear = monthYear
    if(this.buildingId) {
      this.refreshTable(monthYear)
    }
  }

  refreshTable(monthYear: MonthYear) {
    this.waterReadingsService.getAllWaterReadings(this.buildingId, monthYear)
      .subscribe(waterReadings => {
        this.waterReadingsDtos = waterReadings
      })
  }

  openWaterReadingsEdit(waterReadingsDto: WaterReadingsDto) {
    const modalRef = this.modalService.open(WaterReadingsModalComponent)
    modalRef.componentInstance.monthYear = this.monthYear
    modalRef.componentInstance.apartmentNumber = waterReadingsDto.apartmentNumber
    modalRef.componentInstance.model = {coldWater: waterReadingsDto.coldWater, hotWater: waterReadingsDto.hotWater}
    modalRef.componentInstance.callback = (value: { coldWater: number, hotWater: number }) => {
      let request = {...waterReadingsDto}
      request.coldWater = value.coldWater
      request.hotWater = value.hotWater
      this.waterReadingsService.updateWaterReadings(this.buildingId, waterReadingsDto.apartmentId, this.monthYear, request)
        .subscribe(newDto => {
          waterReadingsDto.coldWater = newDto.coldWater
          waterReadingsDto.hotWater = newDto.hotWater
        })
    }
  }

}
