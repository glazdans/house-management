import {Component, OnInit} from '@angular/core'
import {MonthYear} from '@app/month-period/month-year'
import {WaterReadings} from '@app/water-readings/water-readings'
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap'
import {FormBuilder, Validators} from '@angular/forms'
import {ModalComponent} from '@app/core/ui/modal.component'

@Component({
  selector: 'app-water-readings-modal',
  templateUrl: './water-readings-modal.component.html'
})
export class WaterReadingsModalComponent extends ModalComponent implements OnInit {
  apartmentNumber: number
  previousWaterReading: WaterReadings
  monthYear: MonthYear
  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder
  ) {
    super(activeModal)
  }



  ngOnInit(): void {
    this.form = this.formBuilder.group({
      hotWater: [this.model.hotWater, Validators.min(0)],
      coldWater: [this.model.coldWater, Validators.min(0)]
    })
  }

  setValueFields(value: any): any {
  }
}
