import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { Shell } from '@app/shell/shell.service';
import {WaterReadingsListComponent} from '@app/water-readings/water-readings-list/water-readings-list.component'

const routes: Routes = [
  Shell.childRoutes([
    { path: 'water-readings', component: WaterReadingsListComponent, data: { title: extract('Water readings') } },
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WaterReadingsRouteModule { }
