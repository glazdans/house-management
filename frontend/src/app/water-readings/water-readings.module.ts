import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WaterReadingsListComponent} from '@app/water-readings/water-readings-list/water-readings-list.component'
import {WaterReadingsRouteModule} from '@app/water-readings/water-readings-route.module'
import {WaterReadingsService} from '@app/water-readings/water-readings.service'
import {TranslateModule} from "@ngx-translate/core";
import {CoreModule} from "@app/core";
import {SharedModule} from "@app/shared";
import {Angulartics2Module} from "angulartics2";
import {ApartmentListRoutingModule} from "@app/apartment-list/apartment-list-routing.module";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {WaterReadingsModalComponent} from "@app/water-readings/water-readings-modal/water-readings-modal.component";
import {MonthPeriodModule} from "@app/month-period/month-period.module";
import {TableModule} from "primeng/table";


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    Angulartics2Module,
    ApartmentListRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MonthPeriodModule,
    TableModule,
    WaterReadingsRouteModule
  ],
  declarations: [WaterReadingsModalComponent, WaterReadingsListComponent],
  providers: [WaterReadingsService],
  bootstrap: [WaterReadingsModalComponent]
})
export class WaterReadingsModule { }
