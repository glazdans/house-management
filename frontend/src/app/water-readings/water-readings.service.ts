import {Injectable} from '@angular/core'
import {WaterReadings} from '@app/water-readings/water-readings'
import {WaterReadingsDto} from '@app/water-readings/water-readings-dto'
import {Observable, of} from 'rxjs/index'
import {catchError, map} from 'rxjs/internal/operators'
import {HttpCacheService, HttpService} from '@app/core'
import {MonthYear} from '@app/month-period/month-year'

const routes = {
  allWaterReadings: (buildingId: number, monthYear: MonthYear) => {
    return `/building/${buildingId}/water-readings/${monthYear.year}/${monthYear.month}`
  },
  waterReadings: (buildingId: number, apartmentId: number, monthYear: MonthYear) => {
    return `/building/${buildingId}/water-readings/${monthYear.year}/${monthYear.month}/apartment/${apartmentId}`
  }

}

@Injectable()
export class WaterReadingsService {

  constructor(private httpClient: HttpService) {
  }

  public getAllWaterReadings(buildingId: number, monthYear: MonthYear): Observable<WaterReadingsDto[]> {
    return this.httpClient
      .get(routes.allWaterReadings(buildingId, monthYear))
      .pipe(
        map((body: any) => body)
      )
  }

  // TODO change to previous water readings
  public getWaterReadings(buildingId: number, apartmentId: number, monthYear: MonthYear): Observable<WaterReadingsDto> {
    return this.httpClient
      .get(routes.waterReadings(buildingId, apartmentId, monthYear))
      .pipe(
        map((body: any) => body)
      )
  }

  public updateWaterReadings(buildingId: number, apartmentId: number, monthYear: MonthYear, waterReadings: WaterReadingsDto): Observable<WaterReadingsDto> {
    return this.httpClient
      .post(routes.waterReadings(buildingId, apartmentId, monthYear), waterReadings)
      .pipe(
        map((body: any) => body)
      )
  }

}
