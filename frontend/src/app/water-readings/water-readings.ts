import {Apartment} from '@app/apartment-list/apartment.service'
import {MonthPeriod} from '@app/month-period/month-period.service'

export class WaterReadings {
  id: number;
  apartment: Apartment;
  monthPeriod: MonthPeriod;
  hotWater: number;
  coldWater: number;
}
