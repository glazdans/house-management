package lv.glazdans.housemanagement

import lv.glazdans.housemanagement.security.model.User
import lv.glazdans.housemanagement.security.repository.UserRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@Configuration
@Profile("demo")
class DemoDataRunner {

    @Bean
    fun createAdminUser(
        userRepository: UserRepository,
        encoder: BCryptPasswordEncoder
    ) = CommandLineRunner {
        fun createUserIfNotExists(email: String){
            if (userRepository.findByEmail(email) == null) {
                userRepository.save(User(email, encoder.encode("password")))
            }
        }
        createUserIfNotExists("ilze@test.lv")
        createUserIfNotExists("karina@test.lv")

    }
}
