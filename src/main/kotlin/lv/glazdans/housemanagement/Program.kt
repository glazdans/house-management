package lv.glazdans.housemanagement

import lv.glazdans.housemanagement.payments.model.*
import lv.glazdans.housemanagement.payments.repository.MonthPeriodRepository
import lv.glazdans.housemanagement.payments.service.*
import lv.glazdans.housemanagement.reports.BillTemplateService
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.math.BigDecimal
import java.util.*

@SpringBootApplication
@EnableScheduling
class Program : SpringBootServletInitializer() {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(Program::class.java)
        }


        // TODO make this configurable
        @Configuration
        class WebConfig : WebMvcConfigurer {
            override fun addCorsMappings(registry: CorsRegistry) {
                registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "DELETE")
                    .allowedOrigins("*").allowedHeaders("*")
            }
        }
    }
}
