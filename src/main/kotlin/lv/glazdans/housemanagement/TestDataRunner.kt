package lv.glazdans.housemanagement

import lv.glazdans.housemanagement.bill.model.BillField
import lv.glazdans.housemanagement.bill.model.BillProperty
import lv.glazdans.housemanagement.bill.service.BillFieldService
import lv.glazdans.housemanagement.bill.service.BillService
import lv.glazdans.housemanagement.payments.model.*
import lv.glazdans.housemanagement.payments.repository.MonthPeriodRepository
import lv.glazdans.housemanagement.payments.service.*
import lv.glazdans.housemanagement.reports.BillTemplateService
import lv.glazdans.housemanagement.security.model.User
import lv.glazdans.housemanagement.security.repository.UserRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.math.BigDecimal
import java.util.*

@Configuration
@Profile("dev")
class TestDataRunner {

    @Bean
    fun createAdminUser(
        userRepository: UserRepository,
        encoder: BCryptPasswordEncoder
    ) = CommandLineRunner {
        if (userRepository.findAll().isEmpty()) {
            userRepository.save(User("admin", encoder.encode("password")))
        }
    }


    @Bean
    fun init(
        monthPeriodsService: MonthPeriodService,
        apartmentService: ApartmentService,
        waterReadingsService: WaterReadingsService,
        billFieldService: BillFieldService,
        invoiceService: BillService,
        billTemplateService: BillTemplateService,
        buildingService: BuildingService,
        userRepository: UserRepository
    ) = CommandLineRunner {

        val user = userRepository.findByEmail("admin")!!
        val building = buildingService.saveBuilding(
            Building(address = "Brīvības iela 1",
                users = mutableListOf(user),
                landlord = "Brīvības 1",
                regNr = "111111111",
                bank = "Swedbank, AS",
                swift = "HABALV18",
                bankAccount = "LV19HABA0415502145531",
                landlordAddress = "Brīvības iela 1-1"
                ))
/*

        val currentMonthPeriod = monthPeriodsService.getCurrentMonthPeriod(building)
        val oldMonthPeriod = monthPeriodsService.getPreviousMonthPeriod(currentMonthPeriod)
*/

        val apartment =
            apartmentService.addApartment(Apartment(2, BigDecimal("40.99"), "Jānis Bērziņš", "101010-11111", building, null))
        val apartment1 =
            apartmentService.addApartment(Apartment(1, BigDecimal("80.44"), "Pēteris Jaunkalns", "101010-11111", building, null))
/*
        waterReadingsService.addWaterReading(
            WaterReadings(
                apartment,
                oldMonthPeriod,
                BigDecimal(100),
                BigDecimal(100)
            )
        )
        waterReadingsService.addWaterReading(
            WaterReadings(
                apartment,
                currentMonthPeriod,
                BigDecimal("102.22"),
                BigDecimal("103.926")
            )
        )
        waterReadingsService.addWaterReading(
            WaterReadings(
                apartment,
                monthPeriodsService.getPreviousMonthPeriod(oldMonthPeriod),
                BigDecimal("98.22"),
                BigDecimal("96.926")
            )
        )

        waterReadingsService.addWaterReading(
            WaterReadings(
                apartment1,
                oldMonthPeriod,
                BigDecimal(80),
                BigDecimal(50)
            )
        )
        waterReadingsService.addWaterReading(
            WaterReadings(
                apartment1,
                currentMonthPeriod,
                BigDecimal("101"),
                BigDecimal("102")
            )
        )

        waterReadingsService.addWaterReading(
            WaterReadings(
                apartment1,
                monthPeriodsService.getPreviousMonthPeriod(oldMonthPeriod),
                BigDecimal("50"),
                BigDecimal("30")
            )
        )*/

        billFieldService.saveBillField(
            BillField(
                "Apsaimniekošanas maksa",
                BigDecimal("0.3700"), BillProperty.SIZE, defaultField = true, building = building
            )
        )
        billFieldService.saveBillField(
            BillField(
                "Atkritumu izvešana",
                BigDecimal("0.0650"), BillProperty.SIZE, defaultField = true, building = building
            )
        )
        billFieldService.saveBillField(
            BillField(
                "Apkures sistēmas apkope",
                BigDecimal("0.0517"), BillProperty.SIZE, defaultField = true, building = building
            )
        )
        billFieldService.saveBillField(
            BillField(
                "Karstais ūdens uzsildīšana",
                BigDecimal("3.5000"),
                BillProperty.HOT_WATER,
                defaultField = true,
                building = building
            )
        )
        billFieldService.saveBillField(
            BillField(
                "Apkure",
                BigDecimal("0"),
                BillProperty.SIZE,
                defaultField = true,
                building = building
            )
        )
        billFieldService.saveBillField(
            BillField(
                "Aukstais ūdens",
                BigDecimal("1.9239"),
                BillProperty.COLD_WATER,
                defaultField = true,
                building = building
            )
        )
        billFieldService.saveBillField(
            BillField(
                "Ūdens zudumi",
                BigDecimal("-0.0310"),
                BillProperty.SIZE,
                defaultField = true,
                building = building
            )
        )
        billFieldService.saveBillField(
            BillField(
                "Remonta maksājumi",
                BigDecimal("0.1423"),
                BillProperty.SIZE,
                defaultField = true,
                building = building
            )
        )
        billFieldService.saveBillField(
            BillField(
                "Zemes noma",
                BigDecimal("0.1270"),
                BillProperty.SIZE,
                defaultField = true,
                building = building
            )
        )
        billFieldService.saveBillField(
            BillField(
                "Koplietošanas elektrība",
                BigDecimal("0.3600"),
                BillProperty.APARTMENT,
                defaultField = true,
                building = building
            )
        )
        billFieldService.saveBillField(
            BillField(
                "Soda nauda",
                BigDecimal("0"),
                BillProperty.APARTMENT,
                defaultField = true,
                building = building
            )
        )
        billFieldService.saveBillField(
            BillField(
                "Lietus notekūdeņi",
                BigDecimal("0.0050"),
                BillProperty.SIZE,
                defaultField = true,
                building = building
            )
        )
    }

}
