package lv.glazdans.housemanagement.admin.controller

import lv.glazdans.housemanagement.admin.dto.UserDto
import lv.glazdans.housemanagement.admin.service.AdminUserService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/admin/user")
@PreAuthorize("hasRole('ROLE_ADMIN')")
class UserController(val adminUserService: AdminUserService) {

    @GetMapping("")
    fun getUsersList(): List<UserDto> {
        return adminUserService.getAllUsers()
    }

}
