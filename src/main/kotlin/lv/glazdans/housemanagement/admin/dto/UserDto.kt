package lv.glazdans.housemanagement.admin.dto

import lv.glazdans.housemanagement.payments.dto.BuildingDto

data class UserDto(
    val email: String,
    val buildings: List<BuildingDto>,
    val userId: Long
) {
}
