package lv.glazdans.housemanagement.admin.service

import lv.glazdans.housemanagement.admin.dto.UserDto
import lv.glazdans.housemanagement.payments.dto.BuildingDto
import lv.glazdans.housemanagement.payments.model.mapToDto
import lv.glazdans.housemanagement.payments.service.BuildingService
import lv.glazdans.housemanagement.security.repository.UserRepository
import org.springframework.stereotype.Service

@Service
class AdminUserService(
    val userRepository: UserRepository,
    val buildingService: BuildingService
) {

    fun getAllUsers(): List<UserDto> {
        val users = userRepository.findAll()
        return users.map {
            val buildings = buildingService.getUserBuildings(it)
                .map{
                    it.mapToDto()
                }
            UserDto(userId = it.id!!, email = it.email, buildings = buildings)
        }
    }

}
