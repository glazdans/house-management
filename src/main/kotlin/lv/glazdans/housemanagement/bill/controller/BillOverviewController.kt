package lv.glazdans.housemanagement.bill.controller

import lv.glazdans.housemanagement.bill.dto.ApartmentBillOverview
import lv.glazdans.housemanagement.bill.dto.BillFieldSummary
import lv.glazdans.housemanagement.bill.dto.BillOverviewDto
import lv.glazdans.housemanagement.bill.service.BillFieldSummaryService
import lv.glazdans.housemanagement.bill.service.BillOverviewService
import lv.glazdans.housemanagement.payments.ServiceException
import lv.glazdans.housemanagement.payments.service.BuildingService
import lv.glazdans.housemanagement.payments.service.MonthPeriodService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/building/{buildingId}/bill-overview")
class BillOverviewController(
    private val buildingService: BuildingService,
    private val billOverviewService: BillOverviewService,
    private val monthPeriodService: MonthPeriodService,
    private val billFieldSummaryService: BillFieldSummaryService
) {

    @GetMapping("")
    fun getBillList(
        @PathVariable("buildingId") buildingId: Long
    ): List<BillOverviewDto> {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        return billOverviewService.getAllMonths(building)
    }

    @GetMapping("/{monthPeriodId}")
    fun getApartmentBillList(
        @PathVariable("buildingId") buildingId: Long,
        @PathVariable("monthPeriodId") monthPeriodId: Long,
    ): List<ApartmentBillOverview> {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        val monthPeriod = monthPeriodService.getMonthPeriod(monthPeriodId)
        return billOverviewService.getDetailedBillOverview(building, monthPeriod)
    }

    @GetMapping("/{monthPeriodId}/bill-field-summary")
    fun getBillFieldSummary(
        @PathVariable("buildingId") buildingId: Long,
        @PathVariable("monthPeriodId") monthPeriodId: Long,
    ): List<BillFieldSummary> {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        val monthPeriod = monthPeriodService.getMonthPeriod(monthPeriodId)
        return billFieldSummaryService.getBillFieldSummary(monthPeriod)
    }
}
