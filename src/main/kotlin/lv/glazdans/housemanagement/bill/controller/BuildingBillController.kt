package lv.glazdans.housemanagement.bill.controller

import com.haulmont.yarg.reporting.ReportOutputDocument
import lv.glazdans.housemanagement.bill.dto.BillOverviewDto
import lv.glazdans.housemanagement.bill.dto.CreateMonthPeriodDto
import lv.glazdans.housemanagement.bill.service.BillOverviewService
import lv.glazdans.housemanagement.bill.service.BillService
import lv.glazdans.housemanagement.payments.ServiceException
import lv.glazdans.housemanagement.payments.model.BillGenerationResult
import lv.glazdans.housemanagement.payments.model.BillNotification
import lv.glazdans.housemanagement.payments.model.BillResultsType
import lv.glazdans.housemanagement.payments.service.ApartmentService
import lv.glazdans.housemanagement.payments.service.BuildingService
import lv.glazdans.housemanagement.payments.service.MonthPeriodService
import lv.glazdans.housemanagement.reports.BillTemplateService
import lv.glazdans.housemanagement.util.MonthYear
import org.apache.commons.io.IOUtils
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/building/{buildingId}/bill")
class BuildingBillController(
    private val monthPeriodService: MonthPeriodService,
    private val apartmentService: ApartmentService,
    private val billService: BillService,
    private val billTemplateService: BillTemplateService,
    private val buildingService: BuildingService,
    private val billOverviewService: BillOverviewService
) {

    @PostMapping("")
    fun createBillingMonthPeriod(
        @PathVariable("buildingId") buildingId: Long,
        @RequestBody request: CreateMonthPeriodDto
    ): BillOverviewDto {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        val monthPeriod = billService.createBillsAndMonthPeriod(building, request)
        if(request.billNotification != null) {
            billService.saveBillNotification(monthPeriod, BillNotification(request.billNotification))
        }
        return billOverviewService.getMonthOverview(monthPeriod)
    }

    @DeleteMapping("/{monthPeriodId}")
    fun deleteBillingMonthPeriod(
        @PathVariable("buildingId") buildingId: Long,
        @PathVariable("monthPeriodId") monthPeriodId: Long
    ) {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        val monthPeriod = monthPeriodService.getMonthPeriod(monthPeriodId)
        monthPeriodService.deleteMonthPeriod(monthPeriod)
    }

    @RequestMapping(
        "/{monthPeriodId}/apartment/{apartmentId}",
        method = [RequestMethod.GET],
        produces = ["application/vnd.openxmlformats-officedocument.wordprocessingml.document"]
    )
    fun getApartmentBill(
        @PathVariable("buildingId") buildingId: Long,
        @PathVariable("monthPeriodId") monthPeriodId: Long,
        @PathVariable("apartmentId") apartmentId: Long
    ): ResponseEntity<ByteArray> {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        val apartment = apartmentService.getApartment(apartmentId) ?: throw ServiceException("Apartment not found")
        val monthPeriod = monthPeriodService.getMonthPeriod(monthPeriodId)

        val bill = billService.getBillForMonthPeriod(apartment, monthPeriod) ?: throw ServiceException("Bill not found")
        val report = billTemplateService.generateBillForApartment(bill)

        if (report.type == BillResultsType.SUCCESSFUL) {
            val reportOutput = report.value as ReportOutputDocument

            val headers = HttpHeaders()
            headers.contentType =
                MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            headers.setContentDispositionFormData(reportOutput.documentName, reportOutput.documentName)
            headers.cacheControl = "must-revalidate, post-check=0, pre-check=0"
            return ResponseEntity(IOUtils.toByteArray(report.file!!.toURI()), headers, HttpStatus.OK)
        } else {
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }

    }

    @RequestMapping("/{monthPeriodId}", method = [RequestMethod.GET], produces = ["application/zip"])
    fun getAllBills(
        @PathVariable("buildingId") buildingId: Long,
        @PathVariable("monthPeriodId") monthPeriodId: Long,
    ): ResponseEntity<ByteArray> {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        val monthPeriod = monthPeriodService.getMonthPeriod(monthPeriodId)

        val bills = billService.getAllBills(monthPeriod)

        val zipFile = billTemplateService.getAllBillsForMonthPeriod(bills)

        val headers = HttpHeaders()
        headers.contentType = MediaType.parseMediaType("application/zip")
        headers.setContentDispositionFormData(zipFile.name, zipFile.name)
        headers.cacheControl = "must-revalidate, post-check=0, pre-check=0"
        return ResponseEntity(zipFile.readBytes(), headers, HttpStatus.OK)
    }


    // TODO validate apartment fields based on bill fields, to see if they contain required info
    @RequestMapping("/{month}/{year}/apartment/{apartmentId}/error", method = [RequestMethod.GET])
    fun canGenerateApartmentBill(
        @PathVariable("buildingId") buildingId: Long,
        @PathVariable("year") year: Int,
        @PathVariable("month") month: Int,
        @PathVariable("apartmentId") apartmentId: Long
    ): BillGenerationResult {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        val apartment = apartmentService.getApartment(apartmentId) ?: throw ServiceException("Apartment not found")
        val monthPeriod = monthPeriodService.getMonthPeriod(building, MonthYear(month, year))!!

        val bill = billService.getBillForMonthPeriod(apartment, monthPeriod)
        return billTemplateService.generateBillForApartment(bill!!)
    }
}
