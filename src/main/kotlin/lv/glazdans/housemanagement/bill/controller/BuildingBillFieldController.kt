package lv.glazdans.housemanagement.bill.controller

import lv.glazdans.housemanagement.bill.model.BillField
import lv.glazdans.housemanagement.payments.model.BillNotification
import lv.glazdans.housemanagement.bill.service.BillFieldService
import lv.glazdans.housemanagement.bill.service.BillService
import lv.glazdans.housemanagement.payments.ServiceException
import lv.glazdans.housemanagement.payments.service.BuildingService
import lv.glazdans.housemanagement.payments.service.MonthPeriodService
import lv.glazdans.housemanagement.util.MonthYear
import org.springframework.web.bind.annotation.*


@RequestMapping("/building/{buildingId}/bill-field")
@RestController
class BuildingBillFieldController(
    private val billFieldService: BillFieldService,
    private val buildingService: BuildingService
) {

    @RequestMapping(value = [""], method = [RequestMethod.GET])
    fun getBillFields(
        @PathVariable("buildingId") buildingId: Long
    ): List<BillField> {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        return billFieldService.getBuildingsBillFields(building).sortedWith(compareBy { it.name })
    }

    @RequestMapping(value = ["/{billFieldId}"], method = [RequestMethod.GET])
    fun getBillField(@PathVariable("billFieldId") billFieldId: Long): BillField {
        return billFieldService.findById(billFieldId)
    }

    @PostMapping("")
    fun addBillField(
        @PathVariable("buildingId") buildingId: Long,
        @RequestBody billField: BillField
    ): BillField {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        // TODO validate everything
        return billFieldService.saveBillField(billField.copy(building = building))
    }

    @PutMapping("/{billFieldId}")
    fun updateBillField(
        @PathVariable("buildingId") buildingId: Long,
        @RequestBody billField: BillField): BillField {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        // TODO validate everything
        return billFieldService.saveBillField(billField.copy(building = building))
    }

    @RequestMapping(value = ["/{billFieldId}"], method = [RequestMethod.DELETE])
    fun deleteBillField(@RequestBody billField: BillField) {
        billFieldService.deleteBillField(billField)
    }

}
