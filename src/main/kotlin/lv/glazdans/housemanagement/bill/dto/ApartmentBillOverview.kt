package lv.glazdans.housemanagement.bill.dto

import java.math.BigDecimal

data class ApartmentBillOverview(
    val monthPeriodId: Long,
    val apartmentId: Long,
    val apartmentNumber: Int,
    val tenantName: String,
    val billTotal: BigDecimal
) {
}
