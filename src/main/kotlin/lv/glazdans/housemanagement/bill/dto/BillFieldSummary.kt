package lv.glazdans.housemanagement.bill.dto

import java.math.BigDecimal

data class BillFieldSummary(
    val billFieldName: String,
    val total: BigDecimal
)
