package lv.glazdans.housemanagement.bill.dto

import com.fasterxml.jackson.annotation.JsonFormat
import lv.glazdans.housemanagement.payments.model.BillStatus
import java.math.BigDecimal
import java.util.*

data class BillOverviewDto(
    val monthPeriodId: Long,
    val month: Int,
    val year: Int,
    @field:JsonFormat()
    val billDate: Calendar?,
    val allBillTotal: BigDecimal,
    val billStatus: BillStatus
)
