package lv.glazdans.housemanagement.bill.dto

data class CreateMonthPeriodDto(
    val billDate: String,
    val billNotification: String?
)
