package lv.glazdans.housemanagement.bill.model

import lv.glazdans.housemanagement.payments.model.Apartment
import lv.glazdans.housemanagement.payments.model.MonthPeriod
import javax.persistence.*

@Entity
data class Bill(
    @ManyToOne
    val monthPeriod: MonthPeriod,
    @ManyToOne
    val apartment: Apartment,
    val billNumber: String,
    @ManyToMany
    @JoinTable(name = "BILL_BILL_FIELDS")
    val fields: List<BillField>,
    @OneToMany(cascade = [CascadeType.ALL])
    @JoinTable(
        name = "BILL_BILL_FIELD_RESULT",
        joinColumns = [JoinColumn(name = "BILL_FIELD_RESULT_ID", referencedColumnName = "ID")],
        inverseJoinColumns = [JoinColumn(name = "BILL_ID", referencedColumnName = "ID")]
    )
    val fieldResult: List<BillFieldResult>,
    @Id
    @GeneratedValue
    val id: Long? = null
)

