package lv.glazdans.housemanagement.bill.model

import com.fasterxml.jackson.annotation.JsonFormat
import lv.glazdans.housemanagement.payments.model.Apartment
import lv.glazdans.housemanagement.payments.model.Building
import lv.glazdans.housemanagement.payments.model.MonthPeriod
import org.hibernate.annotations.Where
import java.math.BigDecimal
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "BILL_FIELD")
@Where(clause = "deleted = 0")
data class BillField(
    @field:NotNull
    val name: String,

    @Column(precision = 19, scale = 4)
    @field:NotNull
    val tariff: BigDecimal,

    @Enumerated(EnumType.STRING)
    val property: BillProperty,

    val defaultField: Boolean,

    @ManyToOne
    val monthPeriod: MonthPeriod? = null,

    @ManyToMany(fetch = FetchType.EAGER)
    val apartment: List<Apartment> = mutableListOf(),

    @ManyToOne
    @field:NotNull
    val building: Building,

    @field:JsonFormat()
    val validFrom: Calendar? = null,

    @field:JsonFormat()
    val validUntil: Calendar? = null,

    val footerNote: String? = null,

    @Column(name = "deleted")
    var deleted: Int = 0,

    @Id @GeneratedValue val id: Long? = null
)

enum class BillProperty(val measurement: String, val precision: Int) {
    APARTMENT("dzīv.", 0),
    SIZE("m2", 2),
    HOT_WATER("m3", 3),
    COLD_WATER("m3", 3),
    ALL_WATER("m3", 3),
    INHABITANTS("cilv.", 0);
}
