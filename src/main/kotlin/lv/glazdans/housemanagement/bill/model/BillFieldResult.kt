package lv.glazdans.housemanagement.bill.model

import java.math.BigDecimal
import javax.persistence.*

@Entity
data class BillFieldResult(
    @Column(precision = 19, scale = 2)
    val result: BigDecimal,
    @Column(precision = 19, scale = 4)
    val units: BigDecimal,
    @ManyToOne
    val billField: BillField,
    @Id
    @GeneratedValue
    val id: Long? = null

)
