package lv.glazdans.housemanagement.bill.repository

import lv.glazdans.housemanagement.bill.model.BillField
import lv.glazdans.housemanagement.bill.model.BillProperty
import lv.glazdans.housemanagement.payments.model.*
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BillFieldRepository: JpaRepository<BillField, Long> {
    fun findByBuildingAndMonthPeriod(building: Building, monthPeriod: MonthPeriod?): List<BillField>

    fun findByBuildingAndMonthPeriodIsNull(building: Building): List<BillField>

    fun findByBuildingAndMonthPeriodAndDefaultFieldIsTrue(building: Building, monthPeriod: MonthPeriod?): List<BillField>

    fun findByApartmentAndMonthPeriodAndDefaultFieldIsFalse(apartment: Apartment, monthPeriod: MonthPeriod?): List<BillField>

    fun findByMonthPeriodAndProperty(monthPeriod: MonthPeriod, property: BillProperty): List<BillField>
}
