package lv.glazdans.housemanagement.bill.repository

import lv.glazdans.housemanagement.bill.model.BillFieldResult
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BillFieldResultRepository : JpaRepository<BillFieldResult, Long>
