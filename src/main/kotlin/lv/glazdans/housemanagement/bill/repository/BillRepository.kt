package lv.glazdans.housemanagement.bill.repository

import lv.glazdans.housemanagement.bill.model.Bill
import lv.glazdans.housemanagement.payments.model.Apartment
import lv.glazdans.housemanagement.payments.model.MonthPeriod
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BillRepository: JpaRepository<Bill, Long> {
    fun findAllByMonthPeriodIn(monthPeriods: List<MonthPeriod>): List<Bill>
    fun findByMonthPeriodAndApartment(monthPeriods: MonthPeriod, apartment: Apartment): Bill?
}
