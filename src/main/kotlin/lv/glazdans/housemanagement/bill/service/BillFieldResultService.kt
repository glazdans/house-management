package lv.glazdans.housemanagement.bill.service

import lv.glazdans.housemanagement.bill.model.BillFieldResult
import lv.glazdans.housemanagement.payments.MissingFieldException
import lv.glazdans.housemanagement.payments.model.Apartment
import lv.glazdans.housemanagement.bill.model.BillField
import lv.glazdans.housemanagement.bill.model.BillProperty
import lv.glazdans.housemanagement.payments.model.MonthPeriod
import lv.glazdans.housemanagement.payments.service.MonthPeriodService
import lv.glazdans.housemanagement.payments.service.WaterReadingsService
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.math.RoundingMode

@Service
class BillFieldResultService(
    private val waterReadingsService: WaterReadingsService
) {

    fun calculateFieldResults(
        billFields: List<BillField>,
        apartment: Apartment,
        monthPeriod: MonthPeriod
    ): List<BillFieldResult> {
        return billFields.map { billField ->
            val units = getBillFieldPropertyValue(
                billField.property,
                apartment,
                monthPeriod
            )
            val result = (billField.tariff * units).setScale(2, RoundingMode.HALF_UP)

            BillFieldResult(
                result,
                units,
                billField
            )
        }
    }

    private fun getBillFieldPropertyValue(
        property: BillProperty,
        apartment: Apartment,
        monthPeriod: MonthPeriod
    ): BigDecimal {
        return when (property) {
            BillProperty.APARTMENT -> BigDecimal.ONE
            BillProperty.SIZE -> apartment.size
            BillProperty.COLD_WATER -> waterReadingsService
                .getColdWaterDifference(apartment, monthPeriod)
            BillProperty.HOT_WATER -> waterReadingsService
                .getHotWaterDifference(apartment, monthPeriod)
            BillProperty.ALL_WATER -> (waterReadingsService.getHotWaterDifference(apartment, monthPeriod)
                    + waterReadingsService.getColdWaterDifference(apartment, monthPeriod))
            BillProperty.INHABITANTS -> apartment.inhabitantCount?.toBigDecimal()
                ?: throw MissingFieldException("Apartment doesn't have inhabitants specified")
        }
    }
}
