package lv.glazdans.housemanagement.bill.service

import lv.glazdans.housemanagement.bill.model.BillField
import lv.glazdans.housemanagement.bill.model.BillProperty
import lv.glazdans.housemanagement.payments.model.*
import lv.glazdans.housemanagement.bill.repository.BillFieldRepository
import lv.glazdans.housemanagement.payments.service.MonthPeriodService
import lv.glazdans.housemanagement.reports.util.MonthPeriodUtil
import org.apache.commons.collections.ListUtils
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.annotation.Validated
import java.util.*
import javax.validation.Valid

@Service
@Validated
class BillFieldService(
    val billFieldRepository: BillFieldRepository,
    val monthPeriodService: MonthPeriodService
) {
    private fun filterValidUntil(billField: BillField, billDate: Calendar): Boolean {
        val isFromValid = if (billField.validFrom != null) {
            billField.validFrom.before(billDate) || billField.validFrom == billDate
        } else {
            true
        }
        val isUntilValid = if (billField.validUntil != null) {
            billField.validUntil.after(billDate) || billField.validUntil == (billDate)
        } else {
            true
        }
        return isFromValid && isUntilValid
    }

    fun getBuildingsBillFields(building: Building): List<BillField> {
        return billFieldRepository.findByBuildingAndMonthPeriodIsNull(building)
    }

    fun findByMonthPeriod(building: Building, monthPeriod: MonthPeriod): List<BillField> {
        return billFieldRepository.findByBuildingAndMonthPeriod(building, monthPeriod)
    }

    fun findById(billFieldId: Long): BillField {
        return billFieldRepository.findById(billFieldId).get()
    }

    fun findByPropertyField(monthPeriod: MonthPeriod, billProperty: BillProperty): List<BillField> {
        return billFieldRepository.findByMonthPeriodAndProperty(monthPeriod, billProperty)
    }

    fun getValidBillFieldsForPeriod(building: Building, apartment: Apartment): List<BillField> {
        return ListUtils.union(
            billFieldRepository.findByBuildingAndMonthPeriodAndDefaultFieldIsTrue(building, null),
            billFieldRepository.findByApartmentAndMonthPeriodAndDefaultFieldIsFalse(apartment, null)
        ) as List<BillField>
    }

    @Transactional
    fun saveBillField(@Valid billField: BillField): BillField {
        return billFieldRepository.save(billField)
    }

    fun deleteBillField(billField: BillField) {
        billField.deleted = 1
        billFieldRepository.save(billField)
    }

    fun getValidBillFieldsForPeriod(building: Building, billDate: Calendar): List<BillField> {
        return billFieldRepository.findByBuildingAndMonthPeriod(building, null)
            .filter {
                filterValidUntil(it, billDate)
            }
    }

    fun saveCopyOfBillFieldsForMonthPeriod(billFields: List<BillField>, monthPeriod: MonthPeriod): List<BillField> {
        val copies = billFields.map {
            it.copy(
                monthPeriod = monthPeriod,
                apartment = mutableListOf(),
                id = null,
            )
        }
        return billFieldRepository.saveAll(copies)
    }
}
