package lv.glazdans.housemanagement.bill.service

import lv.glazdans.housemanagement.bill.dto.BillFieldSummary
import lv.glazdans.housemanagement.bill.model.BillField
import lv.glazdans.housemanagement.payments.model.MonthPeriod
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class BillFieldSummaryService(
    val billService: BillService
) {

    fun getBillFieldSummary(monthPeriod: MonthPeriod): List<BillFieldSummary> {
        val allBills = billService.getAllBills(monthPeriod)

        val fieldSum = mutableMapOf<BillField, BigDecimal>()
        for (allBill in allBills) {
            allBill.fieldResult.forEach {
                var existingSum = fieldSum[it.billField]
                if(existingSum == null) {
                    existingSum = it.result
                } else {
                    existingSum += it.result
                }
                fieldSum[it.billField] = existingSum
            }
        }
        return fieldSum.map {
            BillFieldSummary(it.key.name, it.value)
        }
    }
}
