package lv.glazdans.housemanagement.bill.service

import lv.glazdans.housemanagement.bill.dto.ApartmentBillOverview
import lv.glazdans.housemanagement.bill.dto.BillOverviewDto
import lv.glazdans.housemanagement.payments.model.Building
import lv.glazdans.housemanagement.payments.model.MonthPeriod
import lv.glazdans.housemanagement.payments.service.ApartmentService
import lv.glazdans.housemanagement.payments.service.MonthPeriodService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal

@Service
class BillOverviewService(
    private val monthPeriodService: MonthPeriodService,
    private val billService: BillService,
    private val apartmentService: ApartmentService
    ) {

    @Transactional
    fun getAllMonths(building: Building): List<BillOverviewDto> {
        val allMonthPeriods = monthPeriodService.getAllMonthPeriods(building)
        return allMonthPeriods.map(this::getMonthOverview)
    }

    fun getMonthOverview(monthPeriod: MonthPeriod): BillOverviewDto {
        return BillOverviewDto(
            monthPeriodId = monthPeriod.id!!,
            month = monthPeriod.month,
            year = monthPeriod.year,
            billDate = monthPeriod.billDate,
            allBillTotal = monthPeriod.billTotal ?: BigDecimal.ZERO,
            billStatus = monthPeriod.billStatus
        )
    }

    fun getDetailedBillOverview(building: Building, monthPeriod: MonthPeriod): List<ApartmentBillOverview> {
        val apartments = apartmentService.getAllApartments(building)
        return apartments.map {
            val bill = billService.getBillForMonthPeriod(it, monthPeriod)!!
            ApartmentBillOverview(
                monthPeriodId = monthPeriod.id!!,
                        apartmentId = it.id!!,
                        apartmentNumber = it.apartmentNumber,
                        tenantName = it.tenantName,
                        billTotal = bill.fieldResult.map{result -> result.result}.reduce { acc, e -> acc + e }
            )
        }
    }
}
