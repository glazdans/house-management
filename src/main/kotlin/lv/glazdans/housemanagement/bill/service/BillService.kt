package lv.glazdans.housemanagement.bill.service

import lv.glazdans.housemanagement.bill.dto.CreateMonthPeriodDto
import lv.glazdans.housemanagement.bill.model.Bill
import lv.glazdans.housemanagement.bill.model.BillField
import lv.glazdans.housemanagement.payments.model.*
import lv.glazdans.housemanagement.bill.repository.BillRepository
import lv.glazdans.housemanagement.payments.service.ApartmentService
import lv.glazdans.housemanagement.payments.service.MonthPeriodService
import lv.glazdans.housemanagement.payments.service.WaterReadingsService
import lv.glazdans.housemanagement.util.MonthPeriodUtil.parseAsCalendar
import mu.KotlinLogging
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class BillService(
    val billRepository: BillRepository,
    val monthPeriodService: MonthPeriodService,
    val billFieldResultService: BillFieldResultService,
    val billFieldService: BillFieldService,
    val apartmentService: ApartmentService,
    val waterReadingsService: WaterReadingsService
) {
    private val logger = KotlinLogging.logger { }

    @Transactional
    fun createBillsAndMonthPeriod(building: Building, request: CreateMonthPeriodDto): MonthPeriod {
        val monthPeriod = monthPeriodService.createMonthPeriodForBill(building, request.billNotification ?: "", request.billDate.parseAsCalendar())
        return this.createBillsForMonthPeriod(building, monthPeriod)
    }

    private fun createBillsForMonthPeriod(building: Building, monthPeriod: MonthPeriod): MonthPeriod {
        val billFields = this.makeCopyOfBillFields(building, monthPeriod)
        waterReadingsService.copyWaterReadings(building, monthPeriod)

        val apartments = apartmentService.getAllApartments(building)
        val bills = apartments.map {
            val apartmentBillFields = filterFieldsForApartment(billFields, it)
            createBillForApartment(apartmentBillFields, it, monthPeriod)
        }
        // TODO crashes when no bill-fields are calculated
        val allBillTotal = bills.map {
            it.fieldResult.map { result ->
                result.result
            }.reduce { acc, e -> acc + e }
        }.reduce { acc, e -> acc + e }

        monthPeriod.billStatus = BillStatus.ACTIVE
        return monthPeriodService.updateBillTotal(monthPeriod, allBillTotal)
    }

    private fun makeCopyOfBillFields(building: Building, monthPeriod: MonthPeriod): List<BillField> {
        val currentBillFields = billFieldService.getValidBillFieldsForPeriod(building, monthPeriod.billDate!!)
        return billFieldService.saveCopyOfBillFieldsForMonthPeriod(currentBillFields, monthPeriod)
    }

    private fun filterFieldsForApartment(billFields: List<BillField>, apartment: Apartment): List<BillField> {
        return billFields.filter {
            it.defaultField ||  it.apartment.any{fieldApartment -> fieldApartment.id == apartment.id}
        }
    }

    private fun createBillForApartment(billFields: List<BillField>, apartment: Apartment, monthPeriod: MonthPeriod): Bill {
        val billFieldResults = billFieldResultService.calculateFieldResults(billFields, apartment, monthPeriod)
        val bill = Bill(monthPeriod, apartment, generateBillNumber(apartment, monthPeriod), billFields, billFieldResults)
        return billRepository.save(bill)
    }

    fun getBillForMonthPeriod(apartment: Apartment, monthPeriod: MonthPeriod): Bill? {
        return billRepository.findByMonthPeriodAndApartment(monthPeriod, apartment)
    }

    fun getAllBills(monthPeriod: MonthPeriod): List<Bill> {
        return billRepository.findAllByMonthPeriodIn(listOf(monthPeriod))

    }

    fun getBills(monthPeriods: List<MonthPeriod>): List<Bill> {
        return billRepository.findAllByMonthPeriodIn(monthPeriods)
    }

    private fun generateBillNumber(apartment: Apartment, monthPeriod: MonthPeriod): String {
        val apartmentPart = apartment.apartmentNumber.toString().padStart(3, '0')
        val monthPart = monthPeriod.year.toString().takeLast(2) + (monthPeriod.month + 1).toString().padStart(2, '0')
        return "01$apartmentPart-$monthPart"
    }

    fun getBillNotification(monthPeriod: MonthPeriod): BillNotification? {
        return monthPeriod.billNotification
    }

    @Transactional
    fun saveBillNotification(monthPeriod: MonthPeriod, billNotification: BillNotification) {
        monthPeriodService.updateBillNotification(monthPeriod, billNotification)
    }
}
