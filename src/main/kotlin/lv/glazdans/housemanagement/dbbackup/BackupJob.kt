package lv.glazdans.housemanagement.dbbackup

import lv.glazdans.housemanagement.util.ZipUtil
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.nio.file.Files
import java.util.*
import java.text.SimpleDateFormat
import kotlin.streams.toList

// TODO delete
@Component
class BackupJob(val dbBackupRepository: HyperSqlDbBackupRepository, val fileBackupService: GoogleDriveFileService){
    private val logger = KotlinLogging.logger {  }

    private val sdf = SimpleDateFormat("yyyy_MM_dd_HH_mm")
//    @Scheduled(cron = "0 0 3 * * ?")
    fun backUpDb(){
        // TODO configureable
        val currentDateString = sdf.format(Date())
        val backupDirectory = Files.createTempDirectory("db_backup_$currentDateString-")
        logger.info { "Created temp directory for backup: $backupDirectory" }
        dbBackupRepository.createBackUp(backupDirectory.toString() + "/")
        val files = Files.walk(backupDirectory)
                .map { it.toFile() }
                .filter {it.isFile}
                .toList()
        val zipFile = ZipUtil.archiveFiles(files)
        logger.info { "Backed up zip - ${zipFile.toURI()}" }
        fileBackupService.saveFile(currentDateString, zipFile, "house-payments-db")
    }
}
