package lv.glazdans.housemanagement.dbbackup

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.drive.Drive
import org.springframework.stereotype.Service
import com.google.api.client.util.store.FileDataStoreFactory
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
import com.google.api.client.http.FileContent
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.services.drive.DriveScopes
import mu.KotlinLogging
import java.io.File
import java.io.IOException
import java.io.InputStreamReader

// TODO https://developers.google.com/drive/api/v3/resumable-upload
@Service
class GoogleDriveFileService{
    private val logger = KotlinLogging.logger {  }

    companion object {
        val CREDENTIALS_FILE_PATH = "/credentials.json"

        val JSON_FACTORY: JacksonFactory = JacksonFactory.getDefaultInstance()
        val SCOPES: List<String> = listOf(DriveScopes.DRIVE_METADATA, DriveScopes.DRIVE_FILE)
        val TOKENS_DIRECTORY_PATH = "tokens"

        private val FOLDER_MIME_TYPE = "application/vnd.google-apps.folder"
    }

    @Throws(IOException::class)
    private fun getCredentials(HTTP_TRANSPORT: NetHttpTransport): Credential {
        // Load client secrets.
        val `in` = GoogleDriveFileService::class.java.getResourceAsStream(CREDENTIALS_FILE_PATH)
        val clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, InputStreamReader(`in`))

        // Build flow and trigger user authorization request.
        val flow = GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(FileDataStoreFactory(java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build()
        val receier = LocalServerReceiver.Builder().setPort(8888).build()
        return AuthorizationCodeInstalledApp(flow, receier).authorize("user")
    }

    fun saveFile(fileName: String, file: File, parentFolder: String){
        val httpTransport: NetHttpTransport = GoogleNetHttpTransport.newTrustedTransport()
        val service = Drive.Builder(httpTransport, JSON_FACTORY, getCredentials(httpTransport))
                .setApplicationName("House-Payments")
                .build()

        // Makes sure that the folder is created
        val folderId = getFolder(parentFolder, service)

        val metadata = com.google.api.services.drive.model.File()
        metadata.name = fileName
        metadata.parents = listOf(folderId)

        val content = FileContent("application/zip", file)
        val result = service.files().create(metadata, content)
                .setFields("id, parents")
                .execute()
        logger.info { "Saved file to google drive - ${result.id}" }
    }

    private fun getFolder(folderName: String, driveService: Drive): String{
        val searchQuery = driveService.files().list()
                .setQ("mimeType='$FOLDER_MIME_TYPE' and name='$folderName'")
                .execute()
        if(searchQuery.files.size > 0){
            return searchQuery.files[0].id
        }

        val folderMetaData = com.google.api.services.drive.model.File()
        folderMetaData.name = folderName
        folderMetaData.mimeType = FOLDER_MIME_TYPE

        val createdFolder = driveService.files().create(folderMetaData)
                .setFields("id")
                .execute()
        return createdFolder.id
    }

}