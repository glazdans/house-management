package lv.glazdans.housemanagement.dbbackup

import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import javax.persistence.EntityManager
import org.springframework.transaction.annotation.Transactional

@Repository
class HyperSqlDbBackupRepository(val em: EntityManager) {

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun createBackUp(@Param("directory") directory: String){
        val nativeQuery = em.createNativeQuery("BACKUP DATABASE TO '$directory' BLOCKING AS FILES")
        nativeQuery.executeUpdate()
    }
}
