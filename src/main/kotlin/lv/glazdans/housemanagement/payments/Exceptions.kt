package lv.glazdans.housemanagement.payments

class MissingFieldException(msg: String): RuntimeException(msg)

class MissingWaterFieldException(val apartmentNumber: Int): RuntimeException()

class ServiceException(val msg: String): RuntimeException(msg)
