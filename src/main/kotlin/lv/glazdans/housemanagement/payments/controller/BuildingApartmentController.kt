package lv.glazdans.housemanagement.payments.controller

import lv.glazdans.housemanagement.payments.ServiceException
import lv.glazdans.housemanagement.payments.model.Apartment
import lv.glazdans.housemanagement.payments.model.WaterReadings
import lv.glazdans.housemanagement.payments.dto.ApartmentDto
import lv.glazdans.housemanagement.payments.service.ApartmentService
import lv.glazdans.housemanagement.payments.service.BuildingService
import lv.glazdans.housemanagement.payments.service.MonthPeriodService
import lv.glazdans.housemanagement.payments.service.WaterReadingsService
import mu.KotlinLogging
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/building/{buildingId}/apartment")
class BuildingApartmentController(
    private val apartmentService: ApartmentService,
    private val waterReadingsService: WaterReadingsService,
    private val monthPeriodService: MonthPeriodService,
    private val buildingService: BuildingService
) {
    private val logger = KotlinLogging.logger { }

    @RequestMapping(
        value = [""],
        params = ["page", "size"],
        method = [(RequestMethod.GET)]
    )
    fun getPaginatedApartments(
        @RequestParam("page") page: Int,
        @RequestParam("size") size: Int,
        @PathVariable("buildingId") buildingId: Long
    ): List<ApartmentDto> {
//        val apartments = apartmentService.getApartments(buildingId, page, size)
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        val apartments = apartmentService.getAllApartments(building)
        return apartments.map { ApartmentDto(it) }
    }


    @RequestMapping(path = ["/"], method = [RequestMethod.POST])
    fun updateApartment(@PathVariable("buildingId") buildingId: Long, @RequestBody apartment: Apartment): Apartment {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        return apartmentService.addApartment(apartment.copy(building = building ))
    }


    @RequestMapping(value = ["/count"], method = [(RequestMethod.GET)])
    fun getApartmentCount(@PathVariable("buildingId") buildingId: Long): Long {
        return apartmentService.getAllApartmentCount(buildingId)

    }

    @RequestMapping(value = ["/"], method = [RequestMethod.GET])
    fun getAllApartments(@PathVariable buildingId: Long): List<Apartment> {
        return apartmentService.getAllApartments(buildingId)
    }


}
