package lv.glazdans.housemanagement.payments.controller

import lv.glazdans.housemanagement.NotFoundException
import lv.glazdans.housemanagement.payments.model.Building
import lv.glazdans.housemanagement.payments.dto.BuildingDto
import lv.glazdans.housemanagement.payments.model.mapToDto
import lv.glazdans.housemanagement.payments.service.BuildingService
import lv.glazdans.housemanagement.security.service.UserService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/building")
class BuildingController(
    val buildingService: BuildingService,
    val userService: UserService
) {

    @GetMapping("/")
    fun getAllUserBuildings(): List<BuildingDto> {
        val user = userService.getUserFromSecurityContext()
        return buildingService.getUserBuildings(user)
            .map {
                it.mapToDto()
            }
    }

    @GetMapping("/{buildingId}")
    fun getBuilding(@PathVariable("buildingId") buildingId: Long): BuildingDto {
        val building = buildingService.getBuilding(buildingId) ?: throw NotFoundException()
        return building.mapToDto()
    }

    @PostMapping("/")
    fun addBuilding(@RequestBody requestBuilding: BuildingDto): BuildingDto {
        val building = Building(
            landlord = requestBuilding.landlord,
            landlordAddress = requestBuilding.landlordAddress,
            regNr = requestBuilding.regNr,
            bank = requestBuilding.bank,
            swift = requestBuilding.swift,
            bankAccount = requestBuilding.bankAccount,
            address = requestBuilding.address,
            users = listOf(userService.getUserFromSecurityContext())
        )
        return buildingService.saveBuilding(building).mapToDto()
    }

    @PutMapping("/{buildingId}")
    fun updateBuilding(@PathVariable("buildingId") buildingId: Long, @RequestBody requestBuilding: BuildingDto): BuildingDto {
        val building = Building(
            address = requestBuilding.address,
            landlord = requestBuilding.landlord,
            landlordAddress = requestBuilding.landlordAddress,
            regNr = requestBuilding.regNr,
            bank = requestBuilding.bank,
            swift = requestBuilding.swift,
            bankAccount = requestBuilding.bankAccount,
            users = listOf(userService.getUserFromSecurityContext()),
            id = buildingId
        )
        return buildingService.saveBuilding(building).mapToDto()
    }

}
