package lv.glazdans.housemanagement.payments.controller

import lv.glazdans.housemanagement.payments.ServiceException
import lv.glazdans.housemanagement.payments.dto.WaterReadingsDto
import lv.glazdans.housemanagement.payments.model.WaterReadings
import lv.glazdans.housemanagement.payments.service.ApartmentService
import lv.glazdans.housemanagement.payments.service.BuildingService
import lv.glazdans.housemanagement.payments.service.MonthPeriodService
import lv.glazdans.housemanagement.payments.service.WaterReadingsService
import lv.glazdans.housemanagement.util.MonthYear
import org.springframework.web.bind.annotation.*

@RequestMapping("/building/{buildingId}/water-readings/{year}/{month}")
@RestController
class WaterReadingsController(
    private val waterReadingsService: WaterReadingsService,
    private val apartmentService: ApartmentService,
    private val buildingService: BuildingService
) {

    @GetMapping("")
    fun getAllWaterReadings(
        @PathVariable("buildingId") buildingId: Long,
        @PathVariable("year") year: Int,
        @PathVariable("month") month: Int
    ): List<WaterReadingsDto> {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        val monthYear = MonthYear(month, year)
        val apartments = apartmentService.getAllApartments(building)

        val waterReadings = waterReadingsService.getWaterReadings(apartments, monthYear)

        return apartments.map { apartment ->
            val existingWaterReadings =
                waterReadings.find { waterReadings -> waterReadings.apartment.id == apartment.id }
            if (existingWaterReadings != null) {
                WaterReadingsDto(existingWaterReadings)
            } else {
                WaterReadingsDto(apartment)
            }
        }
    }

    @RequestMapping("/apartment/{apartmentId}", method = [RequestMethod.GET])
    fun getWaterReadings(
        @PathVariable("buildingId") buildingId: Long,
        @PathVariable("apartmentId") apartmentId: Long,
        @PathVariable("year") year: Int,
        @PathVariable("month") month: Int
    ): WaterReadingsDto? {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        val apartment = apartmentService.getApartment(apartmentId) ?: return null

        val waterReadings = waterReadingsService.getWaterReading(apartment, MonthYear(month, year))
        return if (waterReadings != null) {
            return WaterReadingsDto(waterReadings)
        } else {
            null
        }
    }

    @RequestMapping("/apartment/{apartmentId}", method = [RequestMethod.POST])
    fun updateWaterReadings(
        @PathVariable("buildingId") buildingId: Long,
        @PathVariable("apartmentId") apartmentId: Long,
        @PathVariable("year") year: Int,
        @PathVariable("month") month: Int,
        @RequestBody waterReadings: WaterReadingsDto
    ): WaterReadingsDto {
        val building = buildingService.getBuilding(buildingId) ?: throw ServiceException("Building not found")
        val apartment = apartmentService.getApartment(apartmentId) ?: throw ServiceException("Apartment not found")

        val savedWaterReadings =
            waterReadingsService.updateWaterOrCreateReadings(apartment, MonthYear(month, year), waterReadings)
        return WaterReadingsDto(savedWaterReadings)
    }
}
