package lv.glazdans.housemanagement.payments.dto

import lv.glazdans.housemanagement.payments.model.Apartment
import lv.glazdans.housemanagement.payments.model.WaterReadings

data class ApartmentDto(
    val apartment: Apartment
    )
