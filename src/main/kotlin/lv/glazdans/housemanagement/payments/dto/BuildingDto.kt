package lv.glazdans.housemanagement.payments.dto

data class BuildingDto(
    val address: String,
    val landlord: String,
    val landlordAddress: String,
    val regNr: String,
    val bank: String,
    val swift: String,
    val bankAccount: String,
    val id: Long
)
