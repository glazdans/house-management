package lv.glazdans.housemanagement.payments.dto

import lv.glazdans.housemanagement.payments.model.Apartment
import lv.glazdans.housemanagement.payments.model.WaterReadings
import lv.glazdans.housemanagement.util.NoArg
import java.math.BigDecimal

@NoArg
data class WaterReadingsDto(
    val apartmentId: Long,
    val apartmentNumber: Int,
    val tenantName: String,
    val hotWater: BigDecimal?,
    val coldWater: BigDecimal?
) {
    constructor(apartment: Apartment) : this(
        apartment.id!!,
        apartment.apartmentNumber,
        apartment.tenantName,
        null,
        null
    )


    constructor(apartment: Apartment, hotWater: BigDecimal, coldWater: BigDecimal) : this(
        apartment.id!!,
        apartment.apartmentNumber,
        apartment.tenantName,
        hotWater,
        coldWater
    )

    constructor(waterReadings: WaterReadings) : this(
        waterReadings.apartment,
        waterReadings.hotWater,
        waterReadings.coldWater
    )
}
