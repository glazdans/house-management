package lv.glazdans.housemanagement.payments.model

import com.fasterxml.jackson.annotation.JsonIgnore
import java.math.BigDecimal
import javax.persistence.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Entity
data class Apartment(
        @field:NotNull
        val apartmentNumber: Int,

        @Column(precision = 19, scale = 2)
        @field:NotNull
        val size: BigDecimal,

        @field:NotEmpty
        val tenantName: String,

        val tenantPersonCode: String?,

        @ManyToOne
        @JoinColumn(name = "BUILDING_ID")
        @field:NotNull
        @JsonIgnore
        val building: Building,

        val inhabitantCount: Int?,

        @Id
        @GeneratedValue
        val id: Long? = null)

