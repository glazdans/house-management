package lv.glazdans.housemanagement.payments.model

import lv.glazdans.housemanagement.bill.model.BillField
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "APARTMENT_BILL_FIELDS")
data class ApartementBillFields(
        @EmbeddedId
        val id: ApartmentMonthlyPeriodId,
        @ManyToMany
        val billFields: Set<BillField>)

@Embeddable
data class ApartmentMonthlyPeriodId(@ManyToOne val monthlyPeriod: MonthPeriod,
                                    @ManyToOne val apartment: Apartment) : Serializable
