package lv.glazdans.housemanagement.payments.model

@Deprecated("Goes along with SoftDeleteCrudeRepository")
interface BaseEntity {
    fun getId(): Long?
}