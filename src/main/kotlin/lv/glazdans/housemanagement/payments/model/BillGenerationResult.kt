package lv.glazdans.housemanagement.payments.model

import java.io.File

class BillGenerationResult(val apartment: Apartment, val monthPeriod: MonthPeriod, val type: BillResultsType, val file: File?, val value:Any? = null)

enum class BillResultsType{
    SUCCESSFUL, MISSING_WATER, UNEXPECTED_EXCEPTION, FILE_NOT_FOUND
}
