package lv.glazdans.housemanagement.payments.model

import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "BILL_NOTIFICATION")
data class BillNotification(
        @Lob
        val notification: String = "",
        @Id
        @GeneratedValue
        val id: Long? = null
): Serializable