package lv.glazdans.housemanagement.payments.model

import lv.glazdans.housemanagement.payments.dto.BuildingDto
import lv.glazdans.housemanagement.security.model.User
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
data class Building(
    @field:NotNull
    @Column(name = "STREET")
    val address: String,
    val landlord: String,
    val landlordAddress: String,
    val regNr: String,
    val bank: String,
    val swift: String,
    val bankAccount: String,
    @JoinTable(
        name = "USER_BUILDING",
        joinColumns = [JoinColumn(name = "USER_ID", referencedColumnName = "ID")],
        inverseJoinColumns = [JoinColumn(name = "BUILDING_ID", referencedColumnName = "ID")]
    )
    @ManyToMany
    val users: List<User> = mutableListOf(),
    @OneToMany(mappedBy = "building")
    val apartments: List<Apartment> = mutableListOf(),
    @Id
    @GeneratedValue
    val id: Long? = null
)

fun Building.mapToDto(): BuildingDto {
    return BuildingDto(
        address = this.address,
        landlord = this.landlord,
        landlordAddress = this.landlordAddress,
        regNr = this.regNr,
        bank = this.bank,
        swift = this.swift,
        bankAccount = this.bankAccount,
        id = this.id!!
    )
}
