package lv.glazdans.housemanagement.payments.model

import java.io.Serializable
import java.math.BigDecimal
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Max
import javax.validation.constraints.Min

@Entity
data class MonthPeriod(
    /**
     *  Value range 0-11
     */
    @Min(0)
    @Max(11)
    val month: Int,
    val year: Int,
    @ManyToOne
    val building: Building,
    @Temporal(TemporalType.DATE)
    var billDate: Calendar? = null,

    @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var billNotification: BillNotification? = null,
    @Column(precision = 19, scale = 4)
    var totalWaterMeter: BigDecimal = BigDecimal.ZERO,
    @Column(precision = 19, scale = 2)
    var billTotal: BigDecimal? = null,
    @Column(name = "STATUS")
    @Enumerated(EnumType.ORDINAL)
    var billStatus: BillStatus = BillStatus.NO_BILL,
    @Id @GeneratedValue val id: Long? = null
) : Serializable


enum class BillStatus {
    NO_BILL,
    ACTIVE,
    OLD,
    DELETED;
}
