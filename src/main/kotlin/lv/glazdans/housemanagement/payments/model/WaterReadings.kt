package lv.glazdans.housemanagement.payments.model

import java.math.BigDecimal
import javax.persistence.*
import javax.validation.constraints.Max
import javax.validation.constraints.Min

@Entity
data class WaterReadings(
        @ManyToOne
        val apartment: Apartment,
        @Column(precision = 19, scale = 4) val hotWater: BigDecimal,
        @Column(precision = 19, scale = 4) val coldWater: BigDecimal,
        @Min(0)
        @Max(11)
        val month: Int,
        val year: Int,
        @ManyToOne
        val monthPeriod: MonthPeriod? = null,
        @Id
        @GeneratedValue
        val id: Long? = null)
