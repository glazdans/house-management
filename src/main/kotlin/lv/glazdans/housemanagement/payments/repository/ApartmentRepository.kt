package lv.glazdans.housemanagement.payments.repository

import lv.glazdans.housemanagement.payments.model.Apartment
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ApartmentRepository : JpaRepository<Apartment, Long> {
    fun findAllByBuildingId(buildingId: Long): List<Apartment>
    fun findAllByBuildingId(buildingId: Long, pageable: Pageable): List<Apartment>
    fun countByBuildingId(buildingId: Long): Long
}
