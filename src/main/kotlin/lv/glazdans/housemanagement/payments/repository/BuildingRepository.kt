package lv.glazdans.housemanagement.payments.repository

import lv.glazdans.housemanagement.payments.model.Building
import lv.glazdans.housemanagement.security.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BuildingRepository : JpaRepository<Building, Long>{
    fun findByUsers(user: User): List<Building>
}
