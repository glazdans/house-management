package lv.glazdans.housemanagement.payments.repository

import lv.glazdans.housemanagement.payments.model.Building
import lv.glazdans.housemanagement.payments.model.MonthPeriod
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface MonthPeriodRepository : JpaRepository<MonthPeriod, Long> {
    fun findByMonthAndYear(month: Int, year: Int): MonthPeriod
    fun findAllByBuilding(building: Building): List<MonthPeriod>
    fun findAllByBuildingAndMonthAndYear(building: Building, month: Int, year: Int): List<MonthPeriod>
}
