package lv.glazdans.housemanagement.payments.repository

import lv.glazdans.housemanagement.payments.model.BaseEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.NoRepositoryBean
import org.springframework.transaction.annotation.Transactional


@NoRepositoryBean
@Deprecated("Haven't decided what to do with this")
interface SoftDeleteCrudRepository<T : BaseEntity, ID : Long> : JpaRepository<T, ID> {
    @Transactional(readOnly = true)
    @Query("select e from #{#entityName} e where e.isActive = true")
    override fun findAll(): List<T>

    @Transactional(readOnly = true)
    @Query("select e from #{#entityName} e where e.id in ?1 and e.isActive = true")
    fun findAll(ids: Iterable<ID>): Iterable<T>

    @Transactional(readOnly = true)
    @Query("select e from #{#entityName} e where e.id = ?1 and e.isActive = true")
    fun findOne(id: ID): T?

    //Look up deleted entities
    @Query("select e from #{#entityName} e where e.isActive = false")
    @Transactional(readOnly = true)
    fun findInactive(): List<T>

    @Transactional(readOnly = true)
    @Query("select count(e) from #{#entityName} e where e.isActive = true")
    override fun count(): Long

    @Transactional(readOnly = true)
    fun exists(id: ID): Boolean {
        return findOne(id) != null
    }

    @Query("update #{#entityName} e set e.isActive=false where e.id = ?1")
    @Transactional
    @Modifying
    fun delete(id: Long?)


    @Transactional
    override fun delete(entity: T) {
        delete(entity.getId())
    }

    @Transactional
    fun delete(entities: Iterable<T>) {
        entities.forEach { entitiy -> delete(entitiy.getId()) }
    }

    @Query("update #{#entityName} e set e.isActive=false")
    @Transactional
    @Modifying
    override fun deleteAll()


}