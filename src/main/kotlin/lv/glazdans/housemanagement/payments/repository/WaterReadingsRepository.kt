package lv.glazdans.housemanagement.payments.repository

import lv.glazdans.housemanagement.payments.model.Apartment
import lv.glazdans.housemanagement.payments.model.MonthPeriod
import lv.glazdans.housemanagement.payments.model.WaterReadings
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface WaterReadingsRepository: JpaRepository<WaterReadings, Long> {
    fun findByApartmentAndMonthAndYearAndMonthPeriodIsNull(apartment: Apartment, month: Int, year: Int): List<WaterReadings>
    fun findByApartmentAndMonthAndYearAndMonthPeriod(apartment: Apartment, month: Int, year: Int, monthPeriod: MonthPeriod): WaterReadings?

    fun findByApartmentInAndMonthAndYearAndMonthPeriodIsNull(apartment: List<Apartment>, month: Int, year: Int): List<WaterReadings>

    fun findByApartmentAndMonthPeriod(apartment: Apartment, monthPeriod: MonthPeriod): WaterReadings?
}
