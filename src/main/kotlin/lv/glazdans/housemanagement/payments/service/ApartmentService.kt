package lv.glazdans.housemanagement.payments.service

import lv.glazdans.housemanagement.payments.model.Apartment
import lv.glazdans.housemanagement.payments.model.Building
import lv.glazdans.housemanagement.payments.repository.ApartmentRepository
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service

@Service
class ApartmentService(val apartmentRepository: ApartmentRepository){
    private val logger = KotlinLogging.logger {  }

    fun getAllApartments(buildingId: Long): List<Apartment> {
        return apartmentRepository.findAllByBuildingId(buildingId)
    }

    fun getAllApartments(building: Building): List<Apartment> {
        return apartmentRepository.findAllByBuildingId(building.id!!)
    }

    // TODO use pages that might contain count and wont be required for two request, which is silly
    fun getApartments(buildingId: Long, page: Int, size: Int): List<Apartment> {
        // TODO add search queries
        return apartmentRepository.findAllByBuildingId(buildingId, PageRequest.of(page, size, Sort.by("apartmentNumber")))
    }

    fun getApartment(id: Long):Apartment? {
        return apartmentRepository.findById(id).get()
    }

    fun getAllApartmentCount(buildingId: Long): Long{
        return apartmentRepository.countByBuildingId(buildingId)
    }

    fun addApartment(apartment: Apartment): Apartment{
        // TODO add monthPeriod for history
        return apartmentRepository.save(apartment)
    }

    fun deleteApartment(apartment: Apartment) {
        apartmentRepository.delete(apartment)
    }

}
