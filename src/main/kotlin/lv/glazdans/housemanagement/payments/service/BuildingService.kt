package lv.glazdans.housemanagement.payments.service

import lv.glazdans.housemanagement.payments.model.Building
import lv.glazdans.housemanagement.payments.repository.BuildingRepository
import lv.glazdans.housemanagement.security.model.User
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.util.*

@Service
class BuildingService(val buildingRepository: BuildingRepository) {
    private val logger = KotlinLogging.logger { }

    fun saveBuilding(building: Building): Building {
        return buildingRepository.save(building)
    }

    fun getUserBuildings(user: User): List<Building> {
        return buildingRepository.findByUsers(user)
    }

    fun getBuilding(buildingId: Long): Building? {
        return buildingRepository.findById(buildingId).get()
    }


}
