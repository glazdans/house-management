package lv.glazdans.housemanagement.payments.service

import lv.glazdans.housemanagement.payments.model.BillNotification
import lv.glazdans.housemanagement.payments.model.BillStatus
import lv.glazdans.housemanagement.payments.model.Building
import lv.glazdans.housemanagement.payments.model.MonthPeriod
import lv.glazdans.housemanagement.payments.repository.MonthPeriodRepository
import lv.glazdans.housemanagement.util.MonthPeriodUtil
import lv.glazdans.housemanagement.util.MonthYear
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.*

@Service
class MonthPeriodService(val monthPeriodRepository: MonthPeriodRepository) {

    fun getMonthPeriod(id: Long): MonthPeriod {
        return monthPeriodRepository.getOne(id)
    }

    fun updateBillNotification(monthPeriod: MonthPeriod, billNotification: BillNotification): MonthPeriod {
        monthPeriod.billNotification = billNotification
        return monthPeriodRepository.save(monthPeriod)
    }

    fun updateBillTotal(monthPeriod: MonthPeriod, billTotal: BigDecimal): MonthPeriod {
        monthPeriod.billTotal = billTotal
        return monthPeriodRepository.save(monthPeriod)
    }

    fun getCurrentMonthPeriod(building: Building): MonthPeriod {
        val monthYear = MonthPeriodUtil.getCurrentMonthAndYear()
        return getOrCreateMonthPeriod(building, monthYear)
    }

    fun getMonthPeriod(building: Building, monthYear: MonthYear): MonthPeriod? {
        val monthPeriods =
            monthPeriodRepository.findAllByBuildingAndMonthAndYear(building, monthYear.month, monthYear.year)
        return monthPeriods
            .filter { it.billStatus != BillStatus.DELETED }
            .maxByOrNull { it.billDate!!.time }
    }

    fun getOrCreateMonthPeriod(building: Building, monthYear: MonthYear): MonthPeriod {
        val monthPeriods =
            monthPeriodRepository.findAllByBuildingAndMonthAndYear(building, monthYear.month, monthYear.year)
                .filter {it.billStatus != BillStatus.DELETED}

        return if (monthPeriods.isEmpty()) {
            monthPeriodRepository.save(
                MonthPeriod(
                    month = monthYear.month,
                    year = monthYear.year,
                    building = building
                )
            )
        } else {
            monthPeriods.maxByOrNull { it!!.billDate!!.time }!!
        }
    }

    fun getAllMonthPeriods(building: Building): List<MonthPeriod> {
        return monthPeriodRepository.findAllByBuilding(building).filter { it.billStatus != BillStatus.DELETED }
    }

    fun getPreviousMonthPeriod(monthPeriod: MonthPeriod): MonthPeriod {
        val calendar = Calendar.getInstance()
        calendar.set(monthPeriod.year, monthPeriod.month, 1)
        calendar.add(Calendar.MONTH, -1)

        val monthYear = MonthPeriodUtil.getMonthYear(calendar)
        return getOrCreateMonthPeriod(monthPeriod.building, monthYear)
    }

    fun createMonthPeriodForBill(building: Building, billNotification: String, billDate: Calendar): MonthPeriod {
        val monthYear = MonthPeriodUtil.getMonthYear(billDate)
        val monthPeriods =
            monthPeriodRepository.findAllByBuildingAndMonthAndYear(building, monthYear.month, monthYear.year)
                .filter {it.billStatus != BillStatus.DELETED}
        if (monthPeriods.any { it.billStatus == BillStatus.ACTIVE }) {
            markMonthPeriodsAsOld(monthPeriods)
        }

        val newMonthPeriod = MonthPeriod(
            month = monthYear.month,
            year = monthYear.year,
            building = building,
            billDate = billDate,
            billNotification = BillNotification(billNotification)
        )

        return monthPeriodRepository.save(newMonthPeriod)
    }

    fun markMonthPeriodsAsOld(monthPeriods: List<MonthPeriod>) {
        monthPeriods.forEach {
            monthPeriodRepository.save(
                it.copy(
                    billStatus = BillStatus.OLD
                )
            )
        }
    }

    fun deleteMonthPeriod(monthPeriod: MonthPeriod) {
        monthPeriodRepository.save(monthPeriod.copy(billStatus = BillStatus.DELETED))
    }
}
