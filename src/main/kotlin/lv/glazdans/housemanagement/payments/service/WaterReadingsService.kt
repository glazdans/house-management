package lv.glazdans.housemanagement.payments.service

import lv.glazdans.housemanagement.payments.ServiceException
import lv.glazdans.housemanagement.payments.dto.WaterReadingsDto
import lv.glazdans.housemanagement.payments.model.Apartment
import lv.glazdans.housemanagement.payments.model.Building
import lv.glazdans.housemanagement.payments.model.MonthPeriod
import lv.glazdans.housemanagement.payments.model.WaterReadings
import lv.glazdans.housemanagement.payments.repository.WaterReadingsRepository
import lv.glazdans.housemanagement.util.MonthPeriodUtil.add
import lv.glazdans.housemanagement.util.MonthPeriodUtil.getMonthYear
import lv.glazdans.housemanagement.util.MonthYear
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class WaterReadingsService(
    private val waterReadingsRepository: WaterReadingsRepository,
    private val monthPeriodService: MonthPeriodService,
    private val apartmentService: ApartmentService
) {

    fun getWaterReading(apartment: Apartment, monthYear: MonthYear): WaterReadings? {
        val allWaterReadings =
            waterReadingsRepository.findByApartmentAndMonthAndYearAndMonthPeriodIsNull(
                apartment,
                monthYear.month,
                monthYear.year
            )
        return allWaterReadings.find { it.monthPeriod == null }
    }

    fun getWaterReadings(apartments: List<Apartment>, monthYear: MonthYear): List<WaterReadings> {
        return waterReadingsRepository.findByApartmentInAndMonthAndYearAndMonthPeriodIsNull(apartments, monthYear.month, monthYear.year)
    }

    fun updateWaterOrCreateReadings(
        apartment: Apartment,
        monthYear: MonthYear,
        waterReadings: WaterReadingsDto
    ): WaterReadings {
        val allWaterReadings =
            waterReadingsRepository.findByApartmentAndMonthAndYearAndMonthPeriodIsNull(
                apartment,
                monthYear.month,
                monthYear.year
            )
        val existingWaterReadings = allWaterReadings.find { it.monthPeriod == null }

        return if (existingWaterReadings != null) {
            waterReadingsRepository.save(
                existingWaterReadings.copy(
                    hotWater = waterReadings.hotWater ?: BigDecimal.ZERO,
                    coldWater = waterReadings.coldWater ?: BigDecimal.ZERO
                )
            )
        } else {
            waterReadingsRepository.save(
                WaterReadings(
                    apartment = apartment,
                    hotWater = waterReadings.hotWater ?: BigDecimal.ZERO,
                    coldWater = waterReadings.coldWater ?: BigDecimal.ZERO,
                    month = monthYear.month,
                    year = monthYear.year
                )
            )
        }
    }

    fun getColdWaterDifference(apartment: Apartment, monthPeriod: MonthPeriod): BigDecimal {
        return calculateReadingDifference(
            apartment, monthPeriod
        ) { current, previous ->
            current.coldWater - previous.coldWater
        }
    }

    fun getHotWaterDifference(apartment: Apartment, monthPeriod: MonthPeriod): BigDecimal {
        return calculateReadingDifference(
            apartment, monthPeriod
        ) { current, previous ->
            current.hotWater - previous.hotWater
        }
    }

    private fun calculateReadingDifference(
        apartment: Apartment, monthPeriod: MonthPeriod,
        function: (WaterReadings, WaterReadings) -> BigDecimal
    ): BigDecimal {
        val currentMonthYear = monthPeriod.getMonthYear()
        val firstPeriod = currentMonthYear.add(-1)
        val secondPeriod = currentMonthYear.add(-2)

        val currentReadings = waterReadingsRepository.findByApartmentAndMonthAndYearAndMonthPeriod(
            apartment,
            firstPeriod.month,
            firstPeriod.year,
            monthPeriod
        )
        val previousReadings = waterReadingsRepository.findByApartmentAndMonthAndYearAndMonthPeriod(
            apartment,
            secondPeriod.month,
            secondPeriod.year,
            monthPeriod
        )
        return if (currentReadings != null && previousReadings != null) {
            function.invoke(currentReadings, previousReadings)
        } else {
            BigDecimal.ZERO
        }
    }

    private fun validateWaterReadings(waterReadings: WaterReadings) {
        val previousMonthPeriod = monthPeriodService.getPreviousMonthPeriod(waterReadings.monthPeriod!!)
        val previousWaterReadings =
            waterReadingsRepository.findByApartmentAndMonthPeriod(waterReadings.apartment, previousMonthPeriod);
        if (previousWaterReadings != null) {
            if (waterReadings.hotWater < previousWaterReadings.hotWater) {
                throw ServiceException("Hot water is lower than previous month")
            }
            if (waterReadings.coldWater < previousWaterReadings.coldWater) {
                throw ServiceException("Cold water is lower than previous month")
            }
        }
    }

    fun copyWaterReadings(building: Building, monthPeriod: MonthPeriod) {
        val currentMonthYear = monthPeriod.getMonthYear()
        val firstPeriod = currentMonthYear.add(-1)
        val secondPeriod = currentMonthYear.add(-2)
        val apartments = apartmentService.getAllApartments(building)

        val copies = this.getWaterReadings(apartments, firstPeriod).map {
            it.copy(
                id = null,
                monthPeriod = monthPeriod
            )
        }.union(
            this.getWaterReadings(apartments, secondPeriod).map {
                it.copy(
                    id = null,
                    monthPeriod = monthPeriod
                )
            }
        )
        waterReadingsRepository.saveAll(copies)
    }
}

