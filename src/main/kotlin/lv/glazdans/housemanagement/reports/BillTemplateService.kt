package lv.glazdans.housemanagement.reports

import lv.glazdans.housemanagement.bill.model.Bill
import lv.glazdans.housemanagement.payments.MissingWaterFieldException
import lv.glazdans.housemanagement.payments.model.*
import lv.glazdans.housemanagement.payments.service.ApartmentService
import lv.glazdans.housemanagement.bill.service.BillService
import lv.glazdans.housemanagement.util.ZipUtil
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileNotFoundException


@Service
class BillTemplateService(
    private val billService: BillService,
    private val apartmentService: ApartmentService,
    private val templateGenerationService: TemplateGenerationService,
    private val templateDateService: TemplateDataService
) {
    private val logger = KotlinLogging.logger { }

    // TODO clear temp files
    fun getAllBillsForMonthPeriod(bills: List<Bill>): File {
        val billResults = bills.map { this.generateBillForApartment(it) }
        val files: List<File> = billResults.asSequence().filter { it.file != null }.map { it.file!! }.toList()
        return ZipUtil.archiveFiles(files)
    }

    fun generateBillForApartment(bill: Bill): BillGenerationResult {
        val monthPeriod = bill.monthPeriod
        return try {
            logger.debug { "Creating bill for apartment billResults: ${bill.apartment.apartmentNumber}" }
            val templateData = templateDateService.createTemplateData(bill)

            // TODO this might not work for simultanious execution
            val fileName =
                "./generated_bills/${monthPeriod.year}_${monthPeriod.month}/${bill.apartment.apartmentNumber}.docx"

            val file = File(fileName)
            BillGenerationResult(
                bill.apartment,
                monthPeriod,
                BillResultsType.SUCCESSFUL,
                file,
                templateGenerationService.createReport(templateData, file)
            )
        } catch (e: Exception) {
            when (e) {
                is MissingWaterFieldException -> BillGenerationResult(
                    bill.apartment,
                    monthPeriod,
                    BillResultsType.MISSING_WATER,
                    null
                )
                is FileNotFoundException -> BillGenerationResult(
                    bill.apartment,
                    monthPeriod,
                    BillResultsType.FILE_NOT_FOUND,
                    null,
                    e
                )
                else -> {
                    logger.error(e) { "Can't generate bill" }
                    BillGenerationResult(bill.apartment, monthPeriod, BillResultsType.UNEXPECTED_EXCEPTION, null, e)
                }
            }
        }
    }



}
