package lv.glazdans.housemanagement.reports

import com.fasterxml.jackson.databind.ObjectMapper
import lv.glazdans.housemanagement.bill.model.Bill
import lv.glazdans.housemanagement.bill.model.BillProperty
import lv.glazdans.housemanagement.payments.service.MonthPeriodService
import lv.glazdans.housemanagement.payments.service.WaterReadingsService
import lv.glazdans.housemanagement.reports.util.MonthPeriodUtil
import lv.glazdans.housemanagement.reports.util.MonthToStringUtil
import lv.glazdans.housemanagement.reports.util.moneyToString
import lv.glazdans.housemanagement.util.MonthPeriodUtil.add
import lv.glazdans.housemanagement.util.MonthPeriodUtil.getMonthYear
import org.springframework.stereotype.Service
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

@Service
class TemplateDataService(
    private val monthPeriodService: MonthPeriodService,
    private val waterReadingsService: WaterReadingsService,
) {

    private val jsonParser = ObjectMapper()

    fun createTemplateData(bill: Bill): List<TemplateData> {
        val previousPeriodWaterReadings = waterReadingsService.getWaterReading(
            bill.apartment,
            bill.monthPeriod.getMonthYear().add(-1)
        )

        val totalBillResult = bill.fieldResult.map { it.result }
            .reduce { acc, e -> acc + e }

        val totalJson = jsonParser.writeValueAsString(
            object {
                val name = "Kopā jāmaksā"
                val result = totalBillResult.toString()
            })

        val otherJson = jsonParser.writeValueAsString(
            object {
                val currentDate = MonthToStringUtil.getFormattedCurrentDateInLocative()
                val currentMonthEndDative = MonthToStringUtil.generateLastDayInDativeCase(bill.monthPeriod)
                val currentMonthEndLocative = MonthToStringUtil.generateLastDayInLocativeCase(bill.monthPeriod)
                val currentMonthYearAccusative = MonthToStringUtil.generateMonthYearInAccusative(bill.monthPeriod)
                val shortBillNumber = getShortBillNumber(bill)
                val totalString = moneyToString(totalBillResult)

                val coldWaterLast = previousPeriodWaterReadings?.coldWater
                val hotWaterLast = previousPeriodWaterReadings?.hotWater
                val waterLastDate =
                    MonthPeriodUtil.getLastDate(monthPeriodService.getPreviousMonthPeriod(bill.monthPeriod))
                val waterCurrentDate = MonthPeriodUtil.getLastDate(bill.monthPeriod)

                val notification = getBillNotificationText(bill)
                val tenantAddress = "${bill.apartment.building.address} - ${bill.apartment.apartmentNumber}"
            })

        var count = 1
        val footerNotes = bill.fields.mapNotNull {
            it.footerNote
        }.filter {
            it != ""
        }.map {
            object {
                val text = Collections.nCopies(count++, "*").joinToString("") + it
            }
        }.toList()

        return listOf(
            TemplateData("invoiceFields", getTemplateInvoiceFields(bill)),
            TemplateData("bill", jsonParser.writeValueAsString(bill)),
            TemplateData("total", totalJson),
            TemplateData("other", otherJson),
            TemplateData("footerNotes", jsonParser.writeValueAsString(footerNotes)),
            TemplateData("building", jsonParser.writeValueAsString(bill.apartment.building)),
        )
    }

    private fun getTemplateInvoiceFields(bill: Bill): String {
        var footerCount = 1
        val templateFields = bill.fields.map {
            val footerStars = if (it.footerNote?.isNotBlank() == true) {
                Collections.nCopies(footerCount++, "*").joinToString("")
            } else {
                ""
            }
            val unitParts = getFieldSuperscriptPart(it.property)
            val billFieldResult = bill.fieldResult.filter { result -> result.billField == it }.first()

            TemplateBillFields(
                footerStars + replaceMacro(it.name),
                it.tariff.toString(),
                "${billFieldResult.units.setScale(it.property.precision, RoundingMode.HALF_UP)} ${unitParts.first}",
                unitParts.second,
                billFieldResult.result.toString()
            )
        }

        return jsonParser.writeValueAsString(templateFields)
    }

    private fun replaceMacro(originalString: String): String {
        val p: Pattern = Pattern.compile("\\$\\{([^{}]*)}")
        val m: Matcher = p.matcher(originalString)
        val sb = StringBuffer()
        while (m.find()) {
            val macroKey = m.group(0)
            val formatter = SimpleDateFormat("dd.MM.yyyy")
            val replaceValue = when (macroKey) {
                "\${CurrentMonthPeriod}" -> {
                    val firstDayOfMonth = Calendar.getInstance()
                    firstDayOfMonth.set(Calendar.DAY_OF_MONTH, 1)
                    val lastDayOfMonth = Calendar.getInstance()
                    lastDayOfMonth.set(Calendar.DAY_OF_MONTH, lastDayOfMonth.getActualMaximum(Calendar.DAY_OF_MONTH))
                    "${formatter.format(firstDayOfMonth.time)} - ${formatter.format(lastDayOfMonth.time)}"
                }
                "\${PreviousMonthPeriod}" -> {
                    val firstDayOfMonth = Calendar.getInstance()
                    firstDayOfMonth.add(Calendar.MONTH, -1)
                    firstDayOfMonth.set(Calendar.DAY_OF_MONTH, 1)
                    val lastDayOfMonth = Calendar.getInstance()
                    lastDayOfMonth.add(Calendar.MONTH, -1)
                    lastDayOfMonth.set(Calendar.DAY_OF_MONTH, lastDayOfMonth.getActualMaximum(Calendar.DAY_OF_MONTH))
                    "${formatter.format(firstDayOfMonth.time)} - ${formatter.format(lastDayOfMonth.time)}"
                }
                else -> {
                    ""
                }
            }
            m.appendReplacement(sb, replaceValue)
        }
        return if (sb.isNotEmpty()) {
            sb.toString()
        } else {
            originalString
        }
    }

    private fun getFieldSuperscriptPart(property: BillProperty): Pair<String, String?> {
        val superScriptProperties =
            listOf(BillProperty.ALL_WATER, BillProperty.COLD_WATER, BillProperty.HOT_WATER, BillProperty.SIZE)
        if (superScriptProperties.contains(property)) {
            return Pair(property.measurement.substring(0, 1), property.measurement.substring(1, 2))
        } else {
            return Pair(property.measurement, null)
        }
    }

    private fun getShortBillNumber(bill: Bill): String {
        return bill.billNumber.split("-")[0]
    }

    private fun getBillNotificationText(bill: Bill): String {
        val billNotification = bill.monthPeriod.billNotification
        return billNotification?.notification ?: ""
    }
}


data class TemplateBillFields(
    val name: String,
    val tariff: String,
    val units: String,
    val subscriptUnits: String?,
    val result: String
)

