package lv.glazdans.housemanagement.reports

import org.springframework.stereotype.Service
import java.io.InputStream
import java.net.URI

@Service
class TemplateFileService {

    fun getTemplate(): FileTemplateConfiguration{
        // TODO report configuration
        val resource = BillTemplateService::class.java.getResource("/template/InvoiceTemplate.docx")
        val resourceInputStream = BillTemplateService::class.java.getResourceAsStream("/template/InvoiceTemplate.docx")
        return FileTemplateConfiguration(resource.toURI(), resourceInputStream)
    }

    fun getSecondTemplate(): FileTemplateConfiguration{
        // TODO report configuration
        val resource = BillTemplateService::class.java.getResource("/template/InvoiceTemplate_v2.docx")
        val resourceInputStream = BillTemplateService::class.java.getResourceAsStream("/template/InvoiceTemplate_v2.docx")
        return FileTemplateConfiguration(resource.toURI(), resourceInputStream)
    }
}

class FileTemplateConfiguration(val uri: URI, val inputStream: InputStream)
