package lv.glazdans.housemanagement.reports

import com.fasterxml.jackson.databind.ObjectMapper
import com.haulmont.yarg.formatters.factory.DefaultFormatterFactory
import com.haulmont.yarg.loaders.factory.DefaultLoaderFactory
import com.haulmont.yarg.loaders.impl.GroovyDataLoader
import com.haulmont.yarg.loaders.impl.JsonDataLoader
import com.haulmont.yarg.reporting.ReportOutputDocument
import com.haulmont.yarg.reporting.Reporting
import com.haulmont.yarg.reporting.RunParams
import com.haulmont.yarg.structure.ReportOutputType
import com.haulmont.yarg.structure.impl.BandBuilder
import com.haulmont.yarg.structure.impl.ReportBuilder
import com.haulmont.yarg.structure.impl.ReportFieldFormatImpl
import com.haulmont.yarg.structure.impl.ReportTemplateBuilder
import com.haulmont.yarg.util.groovy.DefaultScriptingImpl
import lv.glazdans.housemanagement.reports.util.CustomFormatterFactory
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileOutputStream

@Service
class TemplateGenerationService(private val templateFileService: TemplateFileService) {
    private val logger = KotlinLogging.logger { }

    fun createReport(templateData: List<TemplateData>, file: File): ReportOutputDocument {
        val reportBuilder = ReportBuilder()

//        val config = templateFileService.getTemplate()
        val config = templateFileService.getSecondTemplate()

        val reportTemplateBuilder = ReportTemplateBuilder()
            .documentContent(config.inputStream)
            .documentPath(config.uri.toString())
            .documentName("InvoiceTemplate.docx")
            .outputType(ReportOutputType.docx)
        reportBuilder.template(reportTemplateBuilder.build())

        val bandBuilder = BandBuilder()

        val templateBands = templateData.map {
            val uppercaseName = it.name.first().uppercase() + it.name.substring(1)
            bandBuilder.name(uppercaseName).query(uppercaseName, "parameter=${it.name}$", "json").build()
        }
        templateBands.forEach {
            reportBuilder.band(it)
        }

        val report = reportBuilder.build()

        val reporting = Reporting()
        val formatterFactory = CustomFormatterFactory()
        reporting.setFormatterFactory(formatterFactory)
        reporting.setLoaderFactory(
            DefaultLoaderFactory()
                .setGroovyDataLoader(GroovyDataLoader(DefaultScriptingImpl()))
                .setJsonDataLoader(JsonDataLoader())
        )

        this.createDirectoriesAndFile(file)

        val reportParams = templateData.associate { it.name to it.json }
        val generatedReport = reporting.runReport(
            RunParams(report)
                .params(reportParams),
            FileOutputStream(file.absoluteFile)
        )
        return generatedReport
    }

    private fun createDirectoriesAndFile(file: File) {
        val parent = file.parentFile
        logger.info {
            "Creating parent $parent directory for ${file.absolutePath}"
        }
        if (parent != null && !parent.exists() && !parent.mkdirs()) {
            throw IllegalStateException("Couldn't create dir: $parent")
        }
        file.createNewFile()
    }
}
