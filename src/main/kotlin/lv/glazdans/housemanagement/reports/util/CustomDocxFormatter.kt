package lv.glazdans.housemanagement.reports.util

import com.haulmont.yarg.formatters.factory.FormatterFactoryInput
import com.haulmont.yarg.formatters.impl.DocxFormatter
import com.haulmont.yarg.formatters.impl.docx.TableManager
import javax.xml.bind.JAXBElement

class CustomDocxFormatter(formatterFactoryInput: FormatterFactoryInput) : DocxFormatter(formatterFactoryInput) {

    override fun fillTables() {
        super.fillTables()
        val tablesToRemove = mutableListOf<TableManager>()
        for (resultingTable in documentWrapper.tables) {
            val rowWithAliases = resultingTable.rowWithAliases
            if (rowWithAliases != null) {
                val bands = rootBand.findBandsRecursively(resultingTable.bandName)
                val hasData = bands.filter {
                    it.data.isNotEmpty()
                }.isNotEmpty()
                if (!hasData) {
                    tablesToRemove.add(resultingTable)
                }
            }
        }
        val mainDoc = wordprocessingMLPackage.mainDocumentPart
        val tables = tablesToRemove.map { it.table }
        mainDoc.content.removeAll(asContentTables(mainDoc.content, tables))
    }

    private fun asContentTables(content: List<Any>, tables: List<Any>): List<Any> {
        val elements = mutableListOf<Any>()
        for (any in content) {
            if (any is JAXBElement<*>) {
                for (table in tables) {
                    if (any.value.equals(table)) {
                        elements.add(any)
                    }
                }
            }
        }
        return elements
    }
}
