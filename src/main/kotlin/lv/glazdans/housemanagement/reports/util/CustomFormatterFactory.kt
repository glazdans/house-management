package lv.glazdans.housemanagement.reports.util

import com.haulmont.yarg.formatters.factory.DefaultFormatterFactory

class CustomFormatterFactory : DefaultFormatterFactory() {
    init {
        val formatter = FormatterCreator {
            val docxFormatter = CustomDocxFormatter(it)
            docxFormatter.setDefaultFormatProvider(defaultFormatProvider)
            docxFormatter.setDocumentConverter(documentConverter)
            docxFormatter.setHtmlImportProcessor(htmlImportProcessor)
            docxFormatter
        }
        formattersMap["docx"] = formatter
    }

}
