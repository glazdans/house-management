package lv.glazdans.housemanagement.reports.util

import java.math.BigDecimal

private const val SPECIAL = "SPECIAL"
private const val WHITESPACE = " "
fun moneyToString(sum: BigDecimal): String {
    if(sum == BigDecimal.ZERO) {
        return "nulle eiro"
    }
    println(sum)
    val stringBuilder = StringBuilder()
    var (integerPart, decimalPart) = sum.toString().split(".")

    if (integerPart.startsWith("-")) {
        integerPart = integerPart.removePrefix("-")
        stringBuilder.append("mīnuss")
    }
    for (i in integerPart.indices) {
        val position = integerPart.length - i

        val positionMap = getPositionMap(position)
        val integerPartString =  integerPart[i].toString()
        val positionString: String? = positionMap[integerPartString]

        if (positionString == SPECIAL) {
            val oneTensCharacters = integerPart[i].toString() + integerPart[i + 1].toString()
            stringBuilder.append(oneTens[oneTensCharacters]).append(WHITESPACE)
            break
        } else if (positionString != ""){
            stringBuilder.append(positionString).append(WHITESPACE)
        }
    }
    stringBuilder.append("eiro").append(WHITESPACE)
    stringBuilder.append(decimalPart).append(WHITESPACE)

    if (decimalPart.last() == '1') {
        stringBuilder.append("cents")
    } else {
        stringBuilder.append("centi")
    }

    return stringBuilder.toString().capitalize()
}

private fun getPositionMap(position: Int): Map<String, String> {
    return when (position) {
        1 -> ones
        2 -> tens
        3 -> hundreds
        4 -> thousands
        else -> throw Exception("Not implemented to 10000 and beyond")
    }
}

private val ones = mapOf(
        Pair("0", ""),
        Pair("1", "viens"),
        Pair("2", "divi"),
        Pair("3", "trīs"),
        Pair("4", "četri"),
        Pair("5", "pieci"),
        Pair("6", "seši"),
        Pair("7", "septiņi"),
        Pair("8", "astoņi"),
        Pair("9", "deviņi")
)
private val tens = mapOf(Pair("0", ""),
        Pair("1", SPECIAL),
        Pair("2", "divdesmit"),
        Pair("3", "trīsdesmit"),
        Pair("4", "četrdesmit"),
        Pair("5", "piecdesmit"),
        Pair("6", "sešdesmit"),
        Pair("7", "septiņdesmit"),
        Pair("8", "astoņdesmit"),
        Pair("9", "deviņdesmit"))
private val hundreds = mapOf(Pair("0", ""),
        Pair("1", "viens simts"),
        Pair("2", "divi simti"),
        Pair("3", "trīs simti"),
        Pair("4", "četri simti"),
        Pair("5", "pieci simti"),
        Pair("6", "seši simti"),
        Pair("7", "septiņi simti"),
        Pair("8", "astoņi simti"),
        Pair("9", "deviņi simti"))

private val thousands = mapOf(Pair("0", ""),
        Pair("1", "viens tūkstotis"),
        Pair("2", "divi tusktoši"),
        Pair("3", "trīs tusktoši"),
        Pair("4", "četri tūsktoši"),
        Pair("5", "pieci tūkstoši"),
        Pair("6", "seši tūsktoši"),
        Pair("7", "septiņi tūkstoši"),
        Pair("8", "astoņi tūkstoši"),
        Pair("9", "deviņi tūkstoši"))

private val oneTens = mapOf(Pair("10", "desmit"),
        Pair("11", "vienpadsmit"),
        Pair("12", "divpadsmit"),
        Pair("13", "trīspadsmit"),
        Pair("14", "četrpadsmit"),
        Pair("15", "piecpadsmit"),
        Pair("16", "sešpadsmit"),
        Pair("17", "septiņpadsmit"),
        Pair("18", "astoņpadsmit"),
        Pair("19", "deviņpadsmit"))