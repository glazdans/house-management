package lv.glazdans.housemanagement.reports.util

import lv.glazdans.housemanagement.payments.model.MonthPeriod
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

object MonthPeriodUtil {
    private val dateFormat = SimpleDateFormat("dd.MM.YYYY.")
    fun getLastDate(monthPeriod: MonthPeriod): String{
        val calendar = getCalendarFromMonthPeriod(monthPeriod)
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
        return dateFormat.format(calendar.time)
    }

    fun getCalendarFromMonthPeriod(monthPeriod: MonthPeriod): Calendar{
        val calendar = Calendar.getInstance()
        calendar.set(monthPeriod.year, monthPeriod.month, 1)
        return calendar
    }

    fun getCalendarFromLocalDate(localDate: LocalDate?): Calendar?{
        if( localDate == null) {
            return null
        }
        val calendar = Calendar.getInstance()
        calendar.set(localDate.year, localDate.month.value - 1, localDate.dayOfMonth)
        return calendar
    }
}
