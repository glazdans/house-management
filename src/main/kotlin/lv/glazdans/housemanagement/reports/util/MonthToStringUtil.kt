package lv.glazdans.housemanagement.reports.util

import lv.glazdans.housemanagement.payments.model.MonthPeriod
import java.util.*

object MonthToStringUtil {

    fun getFormattedCurrentDateInLocative(): String {
        val calendar = Calendar.getInstance()
        return generateDateInLocativeCase(calendar)
    }

    // Datīva locijumā
    fun generateLastDayInDativeCase(monthPeriod: MonthPeriod): String {
        val calendar = Calendar.getInstance()
        calendar.set(monthPeriod.year, monthPeriod.month, 1)
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
        val monthString = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale("lv"))

        return "${calendar.get(Calendar.YEAR)}.gada ${calendar.get(Calendar.DAY_OF_MONTH)}.${dativeCase[monthString]}"
    }

    fun generateLastDayInLocativeCase(monthPeriod: MonthPeriod): String {
        val calendar = Calendar.getInstance()
        calendar.set(monthPeriod.year, monthPeriod.month, 1)
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
        val monthString = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale("lv"))

        return "${calendar.get(Calendar.YEAR)}.gada ${calendar.get(Calendar.DAY_OF_MONTH)}.${lokativeCase[monthString]}"
    }

    fun generateDateInLocativeCase(calendar: Calendar): String {
        val monthString = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale("lv"))

        return "${calendar.get(Calendar.YEAR)}.gada ${calendar.get(Calendar.DAY_OF_MONTH)}.${lokativeCase[monthString]}"
    }

    fun generateMonthYearInAccusative(monthPeriod: MonthPeriod): String {
        val calendar = Calendar.getInstance()
        calendar.set(monthPeriod.year, monthPeriod.month, 1)
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
        val monthString = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale("lv"))

        return "${calendar.get(Calendar.YEAR)}.gada ${accustativeCase[monthString]}"
    }

    private val dativeCase = mapOf(
            Pair("janvāris", "janvārīm"),
            Pair("februāris", "februārim"),
            Pair("marts", "martam"),
            Pair("aprīlis", "aprīlim"),
            Pair("maijs", "maijam"),
            Pair("jūnijs", "jūnijam"),
            Pair("jūlijs", "jūlijam"),
            Pair("augusts", "augustam"),
            Pair("septembris", "septembrim"),
            Pair("oktobris", "oktobrim"),
            Pair("novembris", "novembrim"),
            Pair("decembris", "decembrim")
    )

    private val lokativeCase = mapOf(
            Pair("janvāris", "janvārī"),
            Pair("februāris", "februārī"),
            Pair("marts", "martā"),
            Pair("aprīlis", "aprīlī"),
            Pair("maijs", "maijā"),
            Pair("jūnijs", "jūnijā"),
            Pair("jūlijs", "jūlijā"),
            Pair("augusts", "augustā"),
            Pair("septembris", "septembrī"),
            Pair("oktobris", "oktobrī"),
            Pair("novembris", "novembrī"),
            Pair("decembris", "decembrī")
    )

    private val accustativeCase = mapOf(
            Pair("janvāris", "janvāri"),
            Pair("februāris", "februāri"),
            Pair("marts", "martu"),
            Pair("aprīlis", "aprīli"),
            Pair("maijs", "maiju"),
            Pair("jūnijs", "jūniju"),
            Pair("jūlijs", "jūliju"),
            Pair("augusts", "augustu"),
            Pair("septembris", "septmbri"),
            Pair("oktobris", "oktobri"),
            Pair("novembris", "novembri"),
            Pair("decembris", "decembri")
    )

}