package lv.glazdans.housemanagement.security

import lv.glazdans.housemanagement.security.model.Role
import lv.glazdans.housemanagement.security.repository.RoleRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class RoleSetup {
    @Bean
    fun setUpRoles(
        roleRepository: RoleRepository
    ) = CommandLineRunner {
        RoleList.values().forEach {
            val existingRole = roleRepository.findByName(it.name)
            if(existingRole == null) {
                roleRepository.save(Role(name = it.name))
            }
        }
    }
}
