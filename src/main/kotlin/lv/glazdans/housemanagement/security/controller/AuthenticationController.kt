package lv.glazdans.housemanagement.security.controller

import lv.glazdans.housemanagement.security.jwt.JwtTokenUtil
import lv.glazdans.housemanagement.security.service.UserService
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.AuthenticationException
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/token")
class AuthenticationController(private val userService: UserService,
                               private val jwtTokenUtil: JwtTokenUtil,
                               private val authenticationManager: AuthenticationManager) {


    @RequestMapping(value = ["/generate-token"], method = [RequestMethod.POST])
    @Throws(AuthenticationException::class)
    fun login(@RequestBody loginUser: LoginRequest): ResponseEntity<*> {
        val authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                        loginUser.email,
                        loginUser.password
                )
        )
        SecurityContextHolder.getContext().authentication = authentication
        val user = userService.findOne(loginUser.email)
        val token = jwtTokenUtil.generateToken(user)
        return ResponseEntity.ok<Any>(AuthToken(token))
    }

}

data class AuthToken(val token: String)

data class LoginRequest(val email: String, val password: String)
