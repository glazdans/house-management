package lv.glazdans.housemanagement.security.jwt

object Constants {
    val ACCESS_TOKEN_VALIDITY_SECONDS = (5 * 60 * 60).toLong()
    val SIGNING_KEY = "4gEDpBI8jmN3XJ0GT4eYdul8V57CTXemqkcsHGypMpSHErGbvAsBfxwQlbAF"
    val TOKEN_PREFIX = "Bearer "
    val HEADER_STRING = "Authorization"
}
