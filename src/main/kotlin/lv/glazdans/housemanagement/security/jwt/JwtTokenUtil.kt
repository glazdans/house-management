package lv.glazdans.housemanagement.security.jwt

import org.springframework.security.core.userdetails.UserDetails
import io.jsonwebtoken.Jwts
import org.springframework.security.core.authority.SimpleGrantedAuthority
import io.jsonwebtoken.Claims
import io.jsonwebtoken.SignatureAlgorithm
import lv.glazdans.housemanagement.security.jwt.Constants.ACCESS_TOKEN_VALIDITY_SECONDS
import lv.glazdans.housemanagement.security.jwt.Constants.SIGNING_KEY
import lv.glazdans.housemanagement.security.model.User
import org.springframework.stereotype.Component
import java.io.Serializable
import java.util.*


@Component
class JwtTokenUtil : Serializable {

    fun getUsernameFromToken(token: String): String {
        return getClaimFromToken(token) { claim: Claims -> claim.subject}
    }

    fun getExpirationDateFromToken(token: String): Date {
        return getClaimFromToken(token) { claim: Claims -> claim.expiration}
    }

    fun<T> getClaimFromToken(token: String, claimsResolver: (Claims) -> T): T {
        val claims = getAllClaimsFromToken(token)
        return claimsResolver.invoke(claims)
    }

    private fun getAllClaimsFromToken(token: String): Claims {
        return Jwts.parser()
                .setSigningKey(SIGNING_KEY)
                .parseClaimsJws(token)
                .body
    }

    private fun isTokenExpired(token: String): Boolean {
        val expiration = getExpirationDateFromToken(token)
        return expiration.before(Date())
    }

    fun generateToken(user: User): String {
        return doGenerateToken(user.email)
    }

    private fun doGenerateToken(subject: String): String {

        val claims = Jwts.claims().setSubject(subject)
        // TODO get roles from DB
        claims["scopes"] = Arrays.asList(SimpleGrantedAuthority("ROLE_ADMIN"))

        return Jwts.builder()
                .setClaims(claims)
//                .setIssuer("http://devglan.com")
                .setIssuer("http://localhost")
                .setIssuedAt(Date(System.currentTimeMillis()))
                .setExpiration(Date(System.currentTimeMillis() + ACCESS_TOKEN_VALIDITY_SECONDS * 1000))
                .signWith(SignatureAlgorithm.HS256, SIGNING_KEY)
                .compact()
    }

    fun validateToken(token: String, userDetails: UserDetails): Boolean {
        val username = getUsernameFromToken(token)
        return username == userDetails.username && (!isTokenExpired(token))
    }

}
