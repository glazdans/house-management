package lv.glazdans.housemanagement.security.model

import javax.persistence.*

@Entity
@Table(name = "ROLES")
data class Role(
    val name: String,
    @ManyToMany(mappedBy = "roles")
    val users: List<User> = mutableListOf(),
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long? = null
)
