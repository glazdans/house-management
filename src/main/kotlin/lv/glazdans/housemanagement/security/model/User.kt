package lv.glazdans.housemanagement.security.model

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name = "USERS")
data class User(
    @Column
    val email: String,
    @Column
    @JsonIgnore
    val password: String,
    @ManyToMany
    @JoinTable(
        name = "USERS_ROLES",
        joinColumns = [JoinColumn(
            name = "USERS_ID", referencedColumnName = "ID")],
        inverseJoinColumns = [JoinColumn(
            name = "ROLES_ID", referencedColumnName = "ID")])
    val roles: List<Role> = mutableListOf(),
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long? = null
)
