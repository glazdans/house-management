package lv.glazdans.housemanagement.security.repository

import lv.glazdans.housemanagement.security.model.Role
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoleRepository:JpaRepository<Role, Long> {
    fun findByName(name: String): Role?
}
