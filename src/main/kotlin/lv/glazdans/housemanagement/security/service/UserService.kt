package lv.glazdans.housemanagement.security.service

import lv.glazdans.housemanagement.security.model.User
import lv.glazdans.housemanagement.security.repository.UserRepository
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserService(val userRepository: UserRepository): UserDetailsService {

    fun getUserFromSecurityContext(): User {
        val authentication = SecurityContextHolder.getContext().authentication
        return findOne(authentication.name)
    }

    fun findOne(email: String): User {
        return userRepository.findByEmail(email)!!
    }

    @Transactional
    override fun loadUserByUsername(email: String): UserDetails {
        val user = this.findOne(email)
        val authorities = user.roles.map {
            SimpleGrantedAuthority("ROLE_${it.name}")
        }.toMutableList()
        return object: UserDetails {
            override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
                return authorities
            }

            override fun isEnabled(): Boolean {
                return true
            }

            override fun getUsername(): String {
                return user.email
            }

            override fun isCredentialsNonExpired(): Boolean {
                return true
            }

            override fun getPassword(): String {
                return user.password
            }

            override fun isAccountNonExpired(): Boolean {
                return true
            }

            override fun isAccountNonLocked(): Boolean {
                return true
            }
        }
    }
}
