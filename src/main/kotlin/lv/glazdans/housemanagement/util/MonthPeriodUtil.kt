package lv.glazdans.housemanagement.util

import lv.glazdans.housemanagement.payments.model.MonthPeriod
import java.text.SimpleDateFormat
import java.util.*

object MonthPeriodUtil {
    fun getCurrentMonthAndYear(): MonthYear {
        val calendar = Calendar.getInstance()
        return getMonthYear(calendar)
    }

    fun getMonthYear(calendar: Calendar): MonthYear {
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)
        return MonthYear(month, year)
    }

    fun MonthPeriod.getMonthYear(): MonthYear {
        return MonthYear(this.month, this.year)
    }

    fun MonthYear.asCalendar(): Calendar {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.MONTH, this.month)
        calendar.set(Calendar.YEAR, this.year)
        calendar.set(Calendar.DATE, 1)
        return calendar
    }

    fun String.parseAsCalendar(): Calendar {
        val calendar = Calendar.getInstance()
        calendar.time = SimpleDateFormat("dd.MM.yyyy").parse(this)
        return calendar
    }

    fun MonthYear.add(months: Int): MonthYear {
        val calendar = this.asCalendar()
        calendar.add(Calendar.MONTH, months)
        return getMonthYear(calendar)
    }
}
