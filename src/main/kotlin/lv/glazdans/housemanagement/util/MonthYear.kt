package lv.glazdans.housemanagement.util

data class MonthYear(val month: Int, val year: Int)
