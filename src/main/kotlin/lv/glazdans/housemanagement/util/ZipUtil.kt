package lv.glazdans.housemanagement.util

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.nio.file.Files
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

object ZipUtil {

    fun archiveFiles(files: List<File>): File {
        val zipFile = Files.createTempFile(System.currentTimeMillis().toString(), ".zip")
        val fos = FileOutputStream(zipFile.toFile())
        val zipOut = ZipOutputStream(fos)

        files.forEach {
            val fis = FileInputStream(it)
            val zipEntry = ZipEntry(it.name)
            zipOut.putNextEntry(zipEntry)

            fis.transferTo(zipOut)
            fis.close()
        }

        zipOut.close()
        fos.close()
        return zipFile.toFile()
    }

}