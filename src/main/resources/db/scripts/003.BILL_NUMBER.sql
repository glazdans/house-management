--liquibase formatted sql
--changeset gla:003.BILL_NUMBER
ALTER TABLE BILL
    ADD COLUMN BILL_NUMBER VARCHAR(255);
