--liquibase formatted sql
--changeset gla:021.ADD_MONTH_YEAR_TO_WATER_READINGS
ALTER TABLE WATER_READINGS
    ADD COLUMN MONTH INTEGER NOT NULL;

ALTER TABLE WATER_READINGS
    ADD COLUMN YEAR  INTEGER NOT NULL;

