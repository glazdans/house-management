--liquibase formatted sql
--changeset gla:025.ADD_USER_ROLES_TABLE_01
CREATE TABLE ROLES
(
    NAME VARCHAR(255),
    ID           BIGINT NOT NULL,
    CONSTRAINT PK_ROLES PRIMARY KEY (ID)
);

--changeset gla:025.ADD_USER_ROLES_TABLE_02
CREATE TABLE USERS_ROLES
(
    USERS_ID           BIGINT NOT NULL,
    ROLES_ID           BIGINT NOT NULL,
    CONSTRAINT PK_USERS_ROLES PRIMARY KEY (USERS_ID, ROLES_ID)
);
