package lv.glazdans.housemanagement

import lv.glazdans.housemanagement.payments.model.Building

object TestDataBuilder {

    fun createBuilding(buildingId: Long): Building {
        return Building(
            address = "Brīvības iela 1",
            users = mutableListOf(),
            landlord = "Brīvības 1",
            regNr = "111111111",
            bank = "Swedbank, AS",
            swift = "HABALV18",
            bankAccount = "LV19HABA0415502145531",
            landlordAddress = "Brīvības iela 1-1",
            id = buildingId
        )
    }
}
