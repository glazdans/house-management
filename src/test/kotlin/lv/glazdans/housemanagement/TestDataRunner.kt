package lv.glazdans.housemanagement

import lv.glazdans.housemanagement.bill.model.BillField
import lv.glazdans.housemanagement.bill.model.BillProperty
import lv.glazdans.housemanagement.bill.service.BillFieldService
import lv.glazdans.housemanagement.bill.service.BillService
import org.springframework.stereotype.Component

import lv.glazdans.housemanagement.payments.model.*
import lv.glazdans.housemanagement.payments.service.*
import lv.glazdans.housemanagement.reports.BillTemplateService
import lv.glazdans.housemanagement.security.model.User
import lv.glazdans.housemanagement.security.repository.UserRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.math.BigDecimal

@Profile("test")
@Component
abstract class TestDataRunner(
    private val monthPeriodsService: MonthPeriodService,
    private val apartmentService: ApartmentService,
    private val waterReadingsService: WaterReadingsService,
    private val billFieldService: BillFieldService,
    private val invoiceService: BillService,
    private val billTemplateService: BillTemplateService,
    private val buildingService: BuildingService,
    private val userRepository: UserRepository,
    private val encoder: BCryptPasswordEncoder

) : CommandLineRunner {

//    override fun run(vararg args: String?) {
//        createAdminUser()
//
//        val user = userRepository.findByEmail("admin")
//        val building = buildingService.saveBuilding(Building(address = "Brīvības iela 1", users = mutableListOf(user)))
//
//        val apartment =
//            apartmentService.addApartment(Apartment(1, BigDecimal("40.99"), "Jānis Bērziņš", building, null))
//
//        addWaterReadings(apartment)
//
//        billFieldService.saveBillField(
//            BillField(
//                "Apsaimniekošanas maksa",
//                BigDecimal("0.3700"), BillProperty.SIZE, defaultField = true,
//                building = building
//            )
//        )
//        billFieldService.saveBillField(
//            BillField(
//                "Atkritumu izvešana",
//                BigDecimal("0.0650"), BillProperty.INHABITANTS, defaultField = true,
//                building = building
//            )
//        )
//    }

    fun addWaterReadings(apartment: Apartment) {
 /*       waterReadingsService.addWaterReading(
            WaterReadings(
                apartment,
                BigDecimal(100),
                BigDecimal(100)
            )
        )
        waterReadingsService.addWaterReading(
            WaterReadings(
                apartment,
                BigDecimal("102.22"),
                BigDecimal("103.926")
            )
        )*/

    }

    fun createAdminUser(): User? {

        if (userRepository.findAll().isEmpty()) {
            return userRepository.save(User("admin", encoder.encode("password")))
        }
        return null
    }

}
