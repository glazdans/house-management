package lv.glazdans.housemanagement.bill.service

import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.unmockkAll
import lv.glazdans.housemanagement.TestDataBuilder
import lv.glazdans.housemanagement.bill.model.BillField
import lv.glazdans.housemanagement.bill.model.BillProperty
import lv.glazdans.housemanagement.bill.repository.BillFieldRepository
import lv.glazdans.housemanagement.payments.model.Building
import lv.glazdans.housemanagement.payments.service.MonthPeriodService
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.groups.Tuple
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.util.*

class BillFieldServiceTest {
    private val buildingId = 1L

    @RelaxedMockK
    lateinit var billFieldRepository: BillFieldRepository

    @RelaxedMockK
    lateinit var monthPeriodService: MonthPeriodService
    lateinit var billFieldService: BillFieldService

    @BeforeEach
    fun initMocks() {
        MockKAnnotations.init(this)
        billFieldService = BillFieldService(billFieldRepository, monthPeriodService)
    }

    @AfterEach
    fun cleanUpMocks() {
        unmockkAll()
    }

    @Test
    fun testValidFromValidTo() {
        val building = TestDataBuilder.createBuilding(buildingId)
        val today = Calendar.getInstance()
        val noDateId = 1L
        val validId = 2L
        val invalidBeforeId = 3L
        val invalidAfterId = 4L
        val sameDayId = 5L
        every { billFieldRepository.findByBuildingAndMonthPeriod(building, null) } returns
                listOf(
                    createBillFieldWithDate(building, null, null, noDateId),
                    createBillFieldWithDate(building, today.copyAndAddDays(-1), today.copyAndAddDays(1), validId),
                    createBillFieldWithDate(building, today.copyAndAddDays(1), today.copyAndAddDays(2), invalidBeforeId),
                    createBillFieldWithDate(building, today.copyAndAddDays(-2), today.copyAndAddDays(-1), invalidAfterId),
                    createBillFieldWithDate(building, today.copyAndAddDays(0), today.copyAndAddDays(0), sameDayId),
                )

        val validBillFields = billFieldService.getValidBillFieldsForPeriod(building, today)

        assertThat(validBillFields)
            .extracting(BillField::id)
            .containsOnly(Tuple(noDateId), Tuple(validId), Tuple(sameDayId))
    }

    private fun createBillFieldWithDate(
        building: Building,
        validFrom: Calendar?,
        validTo: Calendar?,
        id: Long
    ): BillField {
        return BillField(
            "Remonta maksājumi",
            BigDecimal("0.1423"),
            BillProperty.SIZE,
            defaultField = true,
            building = building,
            validFrom = validFrom,
            validUntil = validTo,
            id = id
        )
    }

    fun Calendar.copyAndAddDays(days: Int): Calendar {
        val newCalendar = this.clone() as Calendar
        newCalendar.add(Calendar.DATE, days)
        return newCalendar
    }

}
