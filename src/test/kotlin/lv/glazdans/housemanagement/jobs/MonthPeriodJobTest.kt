//package lv.glazdans.housemanagement.jobs
//
//import lv.glazdans.housemanagement.payments.model.MonthPeriod
//import lv.glazdans.housemanagement.payments.repository.MonthPeriodRepository
//import lv.glazdans.housemanagement.payments.service.MonthPeriodService
//import org.junit.jupiter.api.Test
//
//import java.time.LocalDate
//
//@RunWith(MockitoJUnitRunner::class)
//class MonthPeriodJobTest {
//
//    @Test
//    fun testToday() {
//        val monthPeriodService: MonthPeriodService = mock()
//        val monthPeriodRepository: MonthPeriodRepository = mock()
//
//        val testDate = LocalDate.now()
//        val currentMonthPeriod = MonthPeriod(testDate.monthValue - 1, testDate.year)
//        whenever(monthPeriodService.getCurrentMonthPeriod()).thenReturn(currentMonthPeriod)
//        whenever(monthPeriodService.getAllMonthPeriods()).thenReturn(listOf(currentMonthPeriod))
//
//        val monthPeriodJob: MonthPeriodJob = MonthPeriodJob(monthPeriodService, monthPeriodRepository)
//
//        monthPeriodJob.advanceMonthPeriod()
//
//        verify(monthPeriodService, times(0)).advanceMonth()
//    }
//
//    private fun getTestMonthPeriods(testDate: LocalDate, numberOfPeriods: Int): List<MonthPeriod> {
//        return numberOfPeriods.downTo(0).map {
//            val convertedMonthPeriod = testDate.minusMonths(it.toLong())
//            MonthPeriod(convertedMonthPeriod.monthValue - 1, convertedMonthPeriod.year)
//        }
//
//    }
//
//    @Test
//    fun testOneMonthBefore() {
//        val monthPeriodService: MonthPeriodService = mock()
//        val monthPeriodRepository: MonthPeriodRepository = mock()
//
//        var testDate = LocalDate.now()
//
//        val testMonthPeriods = getTestMonthPeriods(testDate, 1)
//        whenever(monthPeriodService.getCurrentMonthPeriod())
//                .thenAnswer(AdditionalAnswers.returnsElementsOf<MonthPeriod>(testMonthPeriods))
//        whenever(monthPeriodService.getAllMonthPeriods()).thenReturn(listOf(testMonthPeriods[0]))
//
//        val monthPeriodJob: MonthPeriodJob = MonthPeriodJob(monthPeriodService, monthPeriodRepository)
//
//        monthPeriodJob.advanceMonthPeriod()
//
//        verify(monthPeriodService, times(1)).advanceMonth()
//    }
//
//    @Test
//    fun testOneYearBefore() {
//        val monthPeriodService: MonthPeriodService = mock()
//        val monthPeriodRepository: MonthPeriodRepository = mock()
//
//        var testDate = LocalDate.now()
//
//        val testMonthPeriods = getTestMonthPeriods(testDate, 12)
//        whenever(monthPeriodService.getCurrentMonthPeriod())
//                .thenAnswer(AdditionalAnswers.returnsElementsOf<MonthPeriod>(testMonthPeriods))
//        whenever(monthPeriodService.getAllMonthPeriods()).thenReturn(listOf(testMonthPeriods[0]))
//
//        val monthPeriodJob: MonthPeriodJob = MonthPeriodJob(monthPeriodService, monthPeriodRepository)
//
//        monthPeriodJob.advanceMonthPeriod()
//
//        verify(monthPeriodService, times(12)).advanceMonth()
//    }
//}
