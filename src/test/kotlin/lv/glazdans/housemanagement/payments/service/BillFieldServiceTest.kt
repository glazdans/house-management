//package lv.glazdans.housemanagement.payments.service
//
//import lv.glazdans.housemanagement.payments.model.Apartment
//import lv.glazdans.housemanagement.bill.model.BillField
//import lv.glazdans.housemanagement.bill.model.BillProperty
//import org.hamcrest.Matchers
//import org.junit.Assert
//import org.junit.Test
//import org.junit.runner.RunWith
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.test.context.SpringBootTest
//import org.springframework.test.context.junit4.SpringRunner
//import java.math.BigDecimal
//import java.util.*
//
//@RunWith(SpringRunner::class)
//@SpringBootTest
//class BillFieldServiceTest{
//    @Autowired
//    lateinit var billFieldService: BillFieldService
//    @Autowired
//    lateinit var monthPeriodService: MonthPeriodService
//    @Autowired
//    lateinit var apartmentService: ApartmentService
//    @Autowired
//    lateinit var apartmentBillFieldService: ApartmentBillFieldsService
//
//    @Test
//    fun testValidUntilExpire(){
//        val currentMonth = monthPeriodService.getCurrentMonthPeriod()
//        val apartment = apartmentService.addApartment(Apartment(1, BigDecimal.ONE,"TEST"))
//        val calendar = Calendar.getInstance()
//        calendar.set(Calendar.DAY_OF_MONTH, 1)
//        calendar.add(Calendar.MONTH, 1)
//        billFieldService.saveBillField(BillField("test", BigDecimal.ONE, BillProperty.SIZE, true, currentMonth,validUntil = calendar))
//
//        Assert.assertThat(apartmentBillFieldService.getApartmentsInvoiceFields(apartment, currentMonth).size, Matchers.`is`(1))
//
//        val firstMonth = monthPeriodService.advanceMonth()
//        billFieldService.copyFromLastMonthPeriod()
//
//        Assert.assertThat(apartmentBillFieldService.getApartmentsInvoiceFields(apartment, firstMonth).size, Matchers.`is`(1))
//
//        val secondMonth = monthPeriodService.advanceMonth()
//        billFieldService.copyFromLastMonthPeriod()
//
//        Assert.assertThat(apartmentBillFieldService.getApartmentsInvoiceFields(apartment, secondMonth).size, Matchers.`is`(0))
//    }
//}
